package com.jameslfc19.situla;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;
import com.jameslfc19.situla.Situla;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Situla";
		cfg.resizable = true;
		cfg.width = 1400;
		cfg.height = 720;
		
//		Settings settings = new Settings();
//        settings.maxWidth = 2048;
//        settings.maxHeight = 2048;
//        settings.filterMin = TextureFilter.Linear;
//        settings.filterMag = TextureFilter.Linear;
//        TexturePacker2.process(settings, "../images", "../Situla-android/assets/images", "situla");
		
		new LwjglApplication(new Situla(new GooglePlayDesktop(), new ShareDesktop()), cfg);
	}
}
