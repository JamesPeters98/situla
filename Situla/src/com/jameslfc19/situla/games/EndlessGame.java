package com.jameslfc19.situla.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.drops.Drop;

public class EndlessGame extends GameScreen {

	public EndlessGame(Game game) {
		super(game,true, 3.5f, 0.05f, 0.9f, false, null);	
		drops.add(DropsRegistry.NormalPairedDouble);
		drops.add(DropsRegistry.NormalFast);
		drops.add(DropsRegistry.Rusty);
		drops.add(DropsRegistry.LifeDrop);
		drops.add(DropsRegistry.SludgeDrop);
	}

	@Override
	protected void preUpdate(float delta) {
	
	}

	@Override
	protected void postUpdate(float delta) {
	
	}

	@Override
	protected void raindropTrigger(float delta, Rectangle raindrop) {
		
	}

	@Override
	protected void postRender(float delta, SpriteBatch batch) {
		// TODO Auto-generated method stub
		
	}	
	
}
