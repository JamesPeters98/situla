package com.jameslfc19.situla.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.interfaces.TutorialItem;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.LevelSelectButton;
import com.jameslfc19.situla.utils.Strings;

public class CampaignGame extends GameScreen{
	
	Button tutorialProceed;
	Image bucketIcon;
	LevelSelectButton levelSelectButton;
	boolean dropTrigger;
	boolean dropTrigger1Complete;
	boolean dropTrigger2Complete;
	boolean dropBucket1Complete;
	boolean dropRight;
	Drop[] drop;

	public CampaignGame(Game game, int catchAmount, float initialDropTime, float dropTimeIncrement, float minDropTime, Drop[] drop, TutorialItem[] tutorialItems, int uniqueID, Screen prevScreen) {
		super(game, false, initialDropTime, dropTimeIncrement, minDropTime, true, prevScreen);
		this.catchAmount = catchAmount;
		this.levelUniqueID = Integer.toString(uniqueID);
		this.drop = drop;
		bucketIcon = new Image(0, 0, 85, 72);
		
		for(TutorialItem item : tutorialItems){
			tutorialItem.add(item);
		}
		
		for(Drop droplet : drop){
			drops.add(droplet);
			if(droplet.chanceDrops != null){
				for(Drop drops : droplet.chanceDrops){
					this.drops.add(drops);
				}
			}
		}
		
	}


	@Override
	protected void preUpdate(float delta) {
		
	}

	@Override
	protected void postUpdate(float delta) {
		
	}

	@Override
	protected void postRender(float delta, SpriteBatch batch) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void raindropTrigger(float delta, Rectangle raindrop) {
		// TODO Auto-generated method stub
		
	}
}
