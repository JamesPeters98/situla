package com.jameslfc19.situla.games;

import java.io.IOException;
import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.LevelData;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.assets.Files;
import com.jameslfc19.situla.buckets.Bucket;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.interfaces.GooglePlayInterface;
import com.jameslfc19.situla.interfaces.TutorialItem;
import com.jameslfc19.situla.screens.EnviromentSelect;
import com.jameslfc19.situla.screens.GameOver;
import com.jameslfc19.situla.screens.GameWin;
import com.jameslfc19.situla.screens.LevelSelector;
import com.jameslfc19.situla.screens.PauseMenu;
import com.jameslfc19.situla.screens.Screens;
import com.jameslfc19.situla.screens.StartMenu;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.ImageCentered;
import com.jameslfc19.situla.utils.Input;
import com.jameslfc19.situla.utils.LevelSelectButton;

public abstract class GameScreen implements Screen {
	
	public final static int FACE_NORMAL = 0;
	public final static int FACE_WIN = 1;
	public final static int FACE_FAIL = 2;
	
	public final int GAME_IN_PROGRESS = 1;
	public final int GAME_OVER = 2;
	public final int GAME_PAUSED = 3;
	public final int GAME_FINISHED = 4;
	public final int GAME_FREEZE = 5;
	public final int GAME_SHOWING_TUTORIAL = 6;
	
	Game game;

	OrthographicCamera camera;
	public Image gameOver, tap, life, pauseicon, tutorialBackground, tutorialSegment, tutIcon, tutTextbar;
	public Sprite bucketTop, bucketBack, bucketBackWin, bucketBackFail, room, room_left, room_right;
	public Bucket bucket;
	public Image button;
	public Button pause;
	public String roomTex, room_leftTex, room_rightTex;
	
	Screen gameWinResumeScreen;
	PauseMenu pauseMenu;
	Vector3 touchPos = new Vector3();
	Array<Drop> drops;
	Array<TutorialItem> tutorialItem;
	Drop normal, rusty, lifedrop;
	float lastDropTime;
	float dropTime;
	float initialDropTime;
	float dropTimeIncrement;
	float dropTimeMin;
	float lastReactionTime;
	float reactionTime = 0.9f;
	float currentTime;
	boolean isCounterRunning;
	boolean isPaused;
	boolean showTutorial;
	
	float deltaTime;
	SpriteBatch batch;
	Vector3 touchPoint;

	int lives;
	int score;
	int highscore;
	int GameState;
	int lastGameState;
	boolean Endless;
	boolean isPressed;
	boolean isCameraRotated;
	float cameraRotationTime;
	
	static int FaceState;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;

	float viewportHeight;
	static float viewportWidth;
	final int WIDTH = 1920;
	final int HEIGHT = 1080;
	
	int catchAmount;
	int currentCatchAmount;
	
	String levelUniqueID;
			
	public GameScreen(Game game, boolean Endless, float initialDropTime, float dropTimeIncrement, float dropTimeMin, boolean showTutorial, Screen prevScreen){		
		drops = new Array<Drop>();
		reset();
		this.game = game;
		this.Endless = Endless;
		this.initialDropTime = initialDropTime;
		this.dropTimeIncrement = dropTimeIncrement;
		this.dropTimeMin = dropTimeMin;
		this.showTutorial = showTutorial;
		
		if(this.showTutorial){
			GameState = GAME_SHOWING_TUTORIAL;
			System.out.println(GameState);
		}
		
		if(Endless){
			highscore = GlobalScores.getInstance().EndlessHighscore;
		}
		
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());

		batch = new SpriteBatch();
		tutorialItem = new Array<TutorialItem>();
		pause = new Button();
		touchPoint = new Vector3();
		
		initImages();
		
		gameWinResumeScreen = prevScreen;
		
		//spawnRaindrop();
	}
	
	private void initImages(){
		bucketTop = Assets.getSprite(Assets.STANDARD_BUCKET_TOP);
		bucketBack = Assets.getSprite(Assets.STANDARD_BUCKET_BACK);
		bucketBackWin = Assets.getSprite(Assets.STANDARD_BUCKET_BACK_WIN);
		bucketBackFail = Assets.getSprite(Assets.STANDARD_BUCKET_BACK_FAIL);
		
		bucket = new Bucket(bucketTop, bucketBack, bucketBackWin, bucketBackFail);
		bucket.setPos(getViewportWidth() / 2 - 256 / 2);
		
		roomTex = Assets.ROOM1;
		room_leftTex = Assets.ROOM1_LEFT;
		room_rightTex = Assets.ROOM1_RIGHT;
		
		room = Assets.getSprite(roomTex, -(getWidth() - getViewportWidth()) / 2, 0);
		room_left = Assets.getSprite(room_leftTex, room.getX() - 497, 0);
		room_right = Assets.getSprite(room_rightTex, room.getX() + room.getWidth() - 2, 0);
		tutorialBackground = new ImageCentered().XandY(822, 1489, viewportHeight, viewportWidth);
		tutorialSegment = new ImageCentered().Y(tutorialBackground.rect.y - 40, 176, 1416, viewportWidth);
		tutIcon = new Image(tutorialSegment.rect.x + 31, tutorialSegment.rect.y + 21, 134, 134);
		tutTextbar = new Image(tutorialSegment.rect.x + 187, tutorialSegment.rect.y + 21, 134, 1200);
		tap = new Image(getViewportWidth() - 112 - 10, getViewportHeight() - 108 - 10, 108, 112);
		life = new Image(getViewportWidth() - 28 - 95, getViewportHeight() - 36 - 128, 36, 28);
		pauseicon =  new Image(15, getViewportHeight() - 260, 84, 76);
	}
	
	public Screen reset(){
		ResetScreenRotation();
		score = 0;
		lives = 3;
		FaceState = FACE_NORMAL;
		currentTime = 0f;
		lastDropTime = dropTime;
		GameState = GAME_IN_PROGRESS;
		isCounterRunning = false;
		currentCatchAmount = 0;
		isCameraRotated = false;
		cameraRotationTime = 0;
		//DropsRegistry.resetDropArrays();
		if(drops != null){
			Iterator<Drop> iter = drops.iterator();
			while(iter.hasNext()){
				Drop drop = iter.next();
				drop.resetArray();
			}
		}
		if(this.showTutorial){
			GameState = GAME_SHOWING_TUTORIAL;
			System.out.println(GameState);
		}
		
		return this;
	}
		
	@Override
	public void render(float delta) {
		//if (delta > 0.1f) delta = 0.1f;
		Gdx.gl.glClearColor(0, 0.7f, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		if(GameState == GAME_PAUSED) pauseGame();
		if(GameState == GAME_IN_PROGRESS){
			isPaused = false;
			gameUpdate(delta);
		}
		if(GameState == GAME_OVER){
			SaveGame();
			if(Endless)Situla.GooglePlay.submitEndlessHighscore(highscore);
			game.setScreen(new GameOver(game, this, score));
		}
		if(GameState == GAME_FINISHED){
			if(levelUniqueID != null){
				if(GlobalScores.getInstance().levelLives.get(levelUniqueID) == null){
					GlobalScores.getInstance().levelLives.put(levelUniqueID, lives);
				}else if(GlobalScores.getInstance().levelLives.get(levelUniqueID) < lives){
					GlobalScores.getInstance().levelLives.put(levelUniqueID, lives);
				}
			}
			SaveGame();
			game.setScreen(new GameWin(game, this, lives, gameWinResumeScreen));
		}

		room.draw(batch);
		room_left.draw(batch);
		room_right.draw(batch);
		batch.draw(Assets.getFilteredTexture("Tap"),  tap.rect.x, tap.rect.y);
		if(lives >= 1)batch.draw(Assets.getFilteredTexture("LifeDropIcon"),  life.rect.x, life.rect.y);
		if(lives >= 2)batch.draw(Assets.getFilteredTexture("LifeDropIcon"),  life.rect.x, life.rect.y - 40);
		if(lives >= 3) batch.draw(Assets.getFilteredTexture("LifeDropIcon"),  life.rect.x, life.rect.y - 80);
		bucket.update(batch, FaceState);
		for(Drop drop : drops){
			Iterator<Rectangle> dropIter = drop.drops.iterator();
			while(dropIter.hasNext()){
				Rectangle raindrop = dropIter.next();
				batch.draw(drop.getSprite(), raindrop.x, raindrop.y);
			}
		}
		Assets.font50.draw(batch, "Score: " + score, 20, 1060);
		if(Endless) Assets.font50.draw(batch, "HighScore: " + highscore, 20, 980);
		if(pause.createIconButton(pauseicon, Assets.getFilteredTexture("PauseIcon"), Assets.getFilteredTexture("PauseIconPressed"), camera, batch, game)){
			lastGameState = GameState;
			GameState = GAME_PAUSED;
		}
		
		if(GameState == GAME_SHOWING_TUTORIAL){
			showTutorial();
		}
		postRender(delta, batch);
		batch.end();
	}
	
	private void showTutorial(){
		batch.draw(Assets.getFilteredTexture("TutorialOverlayBackground"), tutorialBackground.rect.x, tutorialBackground.rect.y);
		Assets.font60.draw(batch, "Tap anywhere to play!", viewportWidth /2 - Assets.font60.getBounds("Tap anywhere to play!").width/2, viewportHeight - 20);
		for(int i = 0; i <= tutorialItem.size-1 && i <= 3; i++){
			TutorialItem item = tutorialItem.get(i);
			float y = (tutorialBackground.rect.y + tutorialBackground.rect.height - tutorialSegment.rect.height - 30 - ((tutorialSegment.rect.height+20) * i));
			float iconY = y + 21 + (tutIcon.rect.height/2) - (item.getBounds().height/2);
			float iconX = tutorialSegment.rect.x + 31 +(tutIcon.rect.width/2) - (item.getBounds().width/2);
			batch.draw(Assets.getFilteredTexture("TutorialOverlaySegment"), tutorialSegment.rect.x , y);
			batch.draw(item.getSprite(), iconX, iconY);
			Assets.font36.setColor(Color.BLACK);
			Assets.font36.drawWrapped(batch, item.getDesrciption(), tutTextbar.rect.x + 40, y + 21 + tutTextbar.rect.height-20, tutTextbar.rect.width - 40);
			Assets.font36.setColor(Color.WHITE);
		}
		if(Gdx.input.isTouched() && Gdx.input.getX() > 100 && !Input.isOtherInputBeingUsed()) GameState = GAME_IN_PROGRESS;
	}
	
	/**
	 * Called at the beginning of the gameUpdate.
	 * 
	 * @param delta
	 */
	protected abstract void preUpdate(float delta);
	
	/**
	 * Called at the end of the gameUpdate.
	 * 
	 * @param delta
	 */
	protected abstract void postUpdate(float delta);
	
	/**
	 * Called at the end of the render.
	 * 
	 * @param delta
	 * @param batch 
	 */
	protected abstract void postRender(float delta, SpriteBatch batch);
	
	/**
	 * Called when each raindrop is iterated. Could be used to check raindrop position and cause triggers etc.
	 * 
	 * @param delta
	 * @param raindrop
	 */
	protected abstract void raindropTrigger(float delta, Rectangle raindrop);
	
	private void gameUpdate(float delta){
		preUpdate(delta);
		if(currentTime == 0) spawnRaindrop();
		currentTime += delta;
		//System.out.println(currentTime);
		
		Input.check(game, this);
		
		if(Input.isEscape() && GameState != GAME_PAUSED){
			lastGameState = GameState;
			GameState = GAME_PAUSED;
		}
		
		if(currentCatchAmount >= catchAmount && !Endless) GameState = GAME_FINISHED;
		

		if(getViewportWidth() > room.getWidth()){
			if (bucket.getX() < room.getX())
				bucket.setPos(room.getX());
			if (bucket.getX() > room.getX() + room.getWidth() - bucket.getWidth())
				bucket.setPos(room.getX() + room.getWidth() - bucket.getWidth());
		} else {
			if (bucket.getX() < 0)
				bucket.setPos(0);
			if (bucket.getX() > getViewportWidth() - bucket.getWidth())
				bucket.setPos(getViewportWidth() - bucket.getWidth());
		}

			dropTime = (initialDropTime - (score * dropTimeIncrement));
		if(dropTime <= 1.5f){
			dropTime = 1.5f;
		}
		
		if (currentTime - lastDropTime > dropTime){
			spawnRaindrop();
			System.out.println("Spawn");
		}
		
		if (currentTime - lastReactionTime > reactionTime){
//			lastReactionTime = currentTime;
			FaceState = FACE_NORMAL;
		}
		
		if(currentTime - cameraRotationTime > dropTime*2 - 0.2f && isCameraRotated){
			System.out.println("Reset camera.");
			RotateScreen();			
		}
		
		iterateDrops(delta);

		if(score > highscore) highscore = score;
		if(lives <= 0) GameState = GAME_OVER;
		checkAcheivements();
		postUpdate(delta);
	}
	
	public void iterateDrops(float delta){
		for(Drop drop : drops){
			Iterator<Rectangle> dropIter = drop.drops.iterator();
			while (dropIter.hasNext()) {
				Rectangle raindrop = dropIter.next();
				raindrop.y -= drop.pixelsPerSecond() * delta;
				raindropTrigger(delta, raindrop);
				if (raindrop.y + raindrop.height < 0) {
					RaindropBelowFloor(raindrop);
					drop.hitFloor(this, dropIter);
				} if (raindrop.x > bucket.getX() + 15 && raindrop.x < bucket.getX() + bucket.getWidth() - raindrop.width - 5 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 220) {
					RaindropInBucket(raindrop);
					drop.landInBucket(this, dropIter);
					if(!Endless)currentCatchAmount ++;
				} else if (raindrop.x > bucket.getX() + 20 - raindrop.width && raindrop.x < bucket.getX() + 15 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 250) {
					RaindropHitBucketSide(raindrop);
					drop.hitBucketSide(this, dropIter);
				} else if (raindrop.x > bucket.getX() + bucket.getWidth() - 20 - raindrop.width && raindrop.x < bucket.getX() + bucket.getWidth() - 20 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 250) {
					RaindropHitBucketSide(raindrop);
					drop.hitBucketSide(this, dropIter);
				}
			}
		}
	}
	
	protected void RaindropBelowFloor(Rectangle raindrop){
		
	}
	protected void RaindropInBucket(Rectangle raindrop){
		
	}
	protected void RaindropHitBucketSide(Rectangle raindrop){
		
	}
	
	private void checkAcheivements(){
		if(score >=5) Situla.GooglePlay.unlockAchievement(Situla.GooglePlay.JustGettingStarted_Ach);
		if(score >=10) Situla.GooglePlay.unlockAchievement(Situla.GooglePlay.GettingBetter_Ach);
		if(score >=50) Situla.GooglePlay.unlockAchievement(Situla.GooglePlay.StillDoBetter_Ach);
	}
	
	public void RotateScreen(){
		if(!isCameraRotated) {
			camera.rotate(180);
			isCameraRotated = true;
			cameraRotationTime = currentTime;
		} else if (isCameraRotated){
			camera.rotate(180);
			isCameraRotated = false;
			cameraRotationTime = currentTime;
		}
	}
	
	public void ResetScreenRotation(){
		if(isCameraRotated){
			camera.rotate(180);
			isCameraRotated = false;
		}
	}
	
	protected void spawnRaindrop(){
		for(Drop drop : drops){
			float x;
			float y;
			if(drop.spawnChance() != 0){
				if(MathUtils.random(1,drop.spawnChance()) == 1){
					drop.dropIncrement();
					Rectangle raindrop = new Rectangle();
					raindrop.width = drop.getRectangleBounds().width;
					raindrop.height = drop.getRectangleBounds().height;
					if(getViewportWidth() < room.getWidth()) x = MathUtils.random(40, getViewportWidth() - raindrop.width - 40);
					else x = MathUtils.random(room.getX() + 40, room.getX() + room.getWidth() - raindrop.width - 40);
					raindrop.x = x;
					y = getHeight();
					raindrop.y = y;
					drop.drops.add(raindrop);
					
					Drop pairedDrop = drop.pairedDrop();
					
					if(pairedDrop != null && drop.isIncrementDrop()){
							Gdx.app.log("Situla", "Is Increment Drop");
							if(drop.dropAmount() >= drop.intervalAmount()){
								Gdx.app.log("Situla", "Drop amount valid");
								System.out.println("Interval Pair");
								Rectangle raindropPair = new Rectangle();
								raindropPair.width = pairedDrop.getRectangleBounds().width;
								raindropPair.height = pairedDrop.getRectangleBounds().height;
								raindropPair.x = x + drop.pairedX();
								raindropPair.y = y + drop.pairedY();
								pairedDrop.drops.add(raindropPair);
								drop.resetDropAmount();
							}
					}else if(drop.pairedChance() != 0){
						if(pairedDrop != null && MathUtils.random(1, drop.pairedChance()) == 1){
							System.out.println("Pair");
							Rectangle raindropPair = new Rectangle();
							raindropPair.width = pairedDrop.getRectangleBounds().width;
							raindropPair.height = pairedDrop.getRectangleBounds().height;
							raindropPair.x = x + drop.pairedX();
							raindropPair.y = y + drop.pairedY();
							pairedDrop.drops.add(raindropPair);	
						}
					}
					
				}
			}
		}
		lastDropTime = currentTime;
	}

	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		this.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		GameScreen.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}
	
	public void addToLives(int lives){
		this.lives += lives;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void addToScore(int add){
		this.score += add; 
	}

	public int getHighscore() {
		return highscore;
	}

	public void setHighscore(int highscore) {
		this.highscore = highscore;
	}

	public int getGameState() {
		return GameState;
	}

	public void setGameState(int gameState) {
		GameState = gameState;
	}

	public int getGameInProgress() {
		return GAME_IN_PROGRESS;
	}

	public int getGameOver() {
		return GAME_OVER;
	}

	public int getGamePaused() {
		return GAME_PAUSED;
	}

	public int getFaceState() {
		return FaceState;
	}

	public void setFaceState(int faceState) {
		FaceState = faceState;
		lastReactionTime = currentTime;
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}
	
	@Override
	public void pause() {
		lastGameState = GameState;
		GameState = GAME_PAUSED;
	}

	@Override
	public void resume() {

	}
	
	public void resumeGame() {
		GameState = GAME_IN_PROGRESS;
	}
	
	public void pauseGame(){
		showPauseMenu();
		if(!isPaused){
			SaveGame();
			pause();
			isPaused = true;
		}
	}
	
	private void showPauseMenu(){
		game.setScreen(new PauseMenu(game, score, highscore, this, lastGameState));
	}
	
	public void SaveGame(){
		if(Endless){
			GlobalScores.getInstance().EndlessHighscore = highscore;
		}
		try {
			Files.writeLevelDataJson();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}
	
	@Override
	public void dispose() {
		System.out.println("Closing!");
		Files.writeToFile("score.dat", Integer.toString(highscore), false);
	}

	@Override
	public void show() {
		Assets.playMusic("Rain", Assets.rain);
		if(!Assets.schemingWeasel.isPlaying()){
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
	}

	@Override
	public void hide() {
		Assets.stopMusic(Assets.rain);
		Assets.stopMusic(Assets.schemingWeasel);
	}
}
