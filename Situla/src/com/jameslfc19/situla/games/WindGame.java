package com.jameslfc19.situla.games;

import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.interfaces.TutorialItem;
import com.jameslfc19.situla.screens.EnviromentSelect;
import com.jameslfc19.situla.screens.Screens;

public class WindGame extends CampaignGame{
	
	float lastWindChange;
	int windInterval = MathUtils.random(5);
	float windDirection = MathUtils.random(-100, 100);

	public WindGame(Game game, int catchAmount, float initialDropTime, float dropTimeIncrement, float minDropTime, Drop[] drop, TutorialItem[] tutorialItems, int uniqueID, Screen prevScreen) {
		super(game, catchAmount, initialDropTime, dropTimeIncrement, minDropTime, drop, tutorialItems, uniqueID, prevScreen);
		
		roomTex = Assets.WINDLEVEL;
		room_leftTex = Assets.WINDLEVEL_LEFT;
		room_rightTex = Assets.WINDLEVEL_RIGHT;
		
		//this.gameWinResumeScreen = Screens.enviromentSelect;
	}
	
	@Override
	public void iterateDrops(float delta){
		for(Drop drop : drops){
			Iterator<Rectangle> dropIter = drop.drops.iterator();
			while (dropIter.hasNext()) {
				Rectangle raindrop = dropIter.next();
				raindrop.y -= drop.pixelsPerSecond() * delta;
				if(getViewportWidth() > room.getWidth()){
					if (raindrop.x <= room.getX() + 40)
						raindrop.x = room.getX() + 40;
					else if (raindrop.x >= room.getX() + room.getWidth() - raindrop.width - 40)
						raindrop.x = room.getX() + room.getWidth() - raindrop.width - 40;
					else raindrop.x += windDirection * delta;
				} else {
					if (raindrop.x <= raindrop.width + 40)
						raindrop.x = raindrop.width + 40;
					else if (raindrop.x >= getViewportWidth() - raindrop.width - 40)
						raindrop.x = getViewportWidth() - raindrop.width - 40;
					else raindrop.x += windDirection * delta;
				}
				raindropTrigger(delta, raindrop);
				if (raindrop.y + raindrop.height < 0) {
					RaindropBelowFloor(raindrop);
					drop.hitFloor(this, dropIter);
				} if (raindrop.x > bucket.getX() + 15 && raindrop.x < bucket.getX() + bucket.getWidth() - raindrop.width - 5 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 220) {
					RaindropInBucket(raindrop);
					drop.landInBucket(this, dropIter);
					if(!Endless)currentCatchAmount ++;
				} else if (raindrop.x > bucket.getX() + 20 - raindrop.width && raindrop.x < bucket.getX() + 15 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 250) {
					RaindropHitBucketSide(raindrop);
					drop.hitBucketSide(this, dropIter);
				} else if (raindrop.x > bucket.getX() + bucket.getWidth() - 20 - raindrop.width && raindrop.x < bucket.getX() + bucket.getWidth() - 20 && raindrop.y > bucket.getY() + 200 && raindrop.y < bucket.getY() + 250) {
					RaindropHitBucketSide(raindrop);
					drop.hitBucketSide(this, dropIter);
				}
			}
		}
	}
	
	@Override
	protected void postRender(float delta, SpriteBatch batch) {
		if(currentTime - lastWindChange > windInterval){
			windInterval = MathUtils.random(5);
			windDirection = MathUtils.random(-100, 100);
			lastWindChange = currentTime;
		}
	}
	
	public void generateNewWindDirection(){
		windDirection = MathUtils.random(-45, 45);
	}

}
