package com.jameslfc19.situla;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.drops.LifeDrop;
import com.jameslfc19.situla.drops.Normal;
import com.jameslfc19.situla.drops.NormalFast;
import com.jameslfc19.situla.drops.Rusty;
import com.jameslfc19.situla.drops.Sludge;

public class DropsRegistry {
	
	public static Drop Normal, NormalPairedDouble, NormalPairedBad, NormalPairedSludge, NormalFast, Rusty, LifeDrop, SludgeDrop;
	
	//static HashMap<Integer, Drop> dropregister = new HashMap<Integer, Drop>();

	public static void register(){
		initialiseDrops();
//		addDrop(Normal);
//		addDrop(NormalPairedDouble);
//		addDrop(NormalPairedBad);
//		addDrop(NormalPairedSludge);
//		addDrop(NormalFast);
//		addDrop(Rusty);
//		addDrop(LifeDrop);
//		addDrop(SludgeDrop);
	}
	
//	public static Drop getDrop(String id){
//		return dropregister.get(id);
//	}
	
//	public static void addDrop(Drop drop){
//		dropregister.put(drop.getId(), drop);
//	}
	
	private static void initialiseDrops(){
		Normal = new Normal();
		
		Drop[] pairedD = {new Rusty(), new Sludge()};
		int[] pairedDChances = {33,66};
		NormalPairedDouble = new Normal(pairedD, pairedDChances, 30);
		
		NormalPairedBad = new Normal(new Rusty(), 20);
		NormalPairedSludge = new Normal(new Sludge(), 10);
		NormalFast = new NormalFast();
		Rusty = new Rusty();
		LifeDrop = new LifeDrop();
		SludgeDrop = new Sludge();
	}
	
//	public static void resetDropArrays(){
//		Iterator<Entry<Integer, Drop>> iter = dropregister.entrySet().iterator();
//		while(iter.hasNext()){
//			Entry<Integer, Drop> entry = iter.next();
//			entry.getValue().resetArray();
//		}
//	}
}
