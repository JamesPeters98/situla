package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class Resize {
	
	public static void set1080(Game game, boolean fullscreen){
		game.resize(1920, 1080);
		Gdx.graphics.setDisplayMode(1920, 1080, fullscreen);
	}
	
	public static void set720(Game game, boolean fullscreen){
		game.resize(1280, 720);
		Gdx.graphics.setDisplayMode(1280, 720, fullscreen);
	}
	
	public static void set360(Game game, boolean fullscreen){
		game.resize(640, 360);
		Gdx.graphics.setDisplayMode(640, 360, fullscreen);
	}
}
