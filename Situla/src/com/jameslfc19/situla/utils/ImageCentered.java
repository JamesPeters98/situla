package com.jameslfc19.situla.utils;

import com.badlogic.gdx.math.Rectangle;

public class ImageCentered extends Image{
	
	public ImageCentered(){
		super(0, 0, 0, 0);
	}

	public Image Y(float y, int height, int width, float viewportWidth){
		rect = new Rectangle();
		rect.x = (viewportWidth /2) - (width/2);
		rect.y = y;
		rect.height = height;
		rect.width = width;
		return this;
	}
	

	public Image XandY(int height, int width, float viewportHeight, float viewportWidth){
		rect = new Rectangle();
		rect.x = (viewportWidth /2) - (width/2);
		rect.y = (viewportHeight /2) - (height/2);
		rect.height = height;
		rect.width = width;
		return this;
	}

}
