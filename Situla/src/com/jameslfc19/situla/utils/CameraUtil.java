package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class CameraUtil {
	 final static float HEIGHT = 1080;
	 final static float WIDTH = 1920;
	 static float physWidth;
	 static float physHeight;
	
	 static float viewportWidth;
	 static float viewportHeight;
	 
	public static OrthographicCamera resizeCamera(OrthographicCamera camera){
		calculateAspectRatio();
		camera.setToOrtho(false, viewportWidth, viewportHeight);
		return camera;
	}
	
	private static void calculateAspectRatio(){
		float aspect = WIDTH / HEIGHT;
		physWidth = Gdx.graphics.getWidth();
		physHeight = Gdx.graphics.getHeight();

		if (physWidth / physHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physWidth / physHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physHeight / physWidth;
		}
	}

}
