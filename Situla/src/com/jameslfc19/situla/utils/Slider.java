package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.jameslfc19.situla.assets.Assets;

public class Slider {
	
	private double sliderPercentage;
	private boolean touched;
	
	public double getSliderPercentage(){
		return sliderPercentage;
	}
	
	public void createSlider(Image sliderBar, Image slider, int MinX, int MaxX, SpriteBatch batch, OrthographicCamera camera, double sensitivityPercentage){
		double sliderRange = (sliderBar.rect.x + sliderBar.rect.width - MaxX)-(sliderBar.rect.x + MinX);
		double multiplier = 100/sensitivityPercentage;
		double sliderPos = sliderRange/multiplier + sliderBar.rect.x + MinX;
		System.out.println("SliderPos: "+sliderPos);
		slider.rect.x = (float) sliderPos;
		
		
		batch.draw(Assets.getFilteredTexture("SliderBarBackground"), sliderBar.rect.x, sliderBar.rect.y);
		if(Gdx.input.isTouched() && !Input.isOtherInputBeingUsed() && Input.checkIfTouchedInRectangle(sliderBar, camera)){
			touched = true;
		}
		
		if(touched){
			Vector3 touchPoint = new Vector3();
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			slider.rect.x = touchPoint.x;
			Gdx.app.log("[Debug]", "Inside rectangle.");
			touched = true;
			Input.setOtherInputBeingUsed(true);	
		}	
		
		if(!Gdx.input.isTouched()){
			Input.setOtherInputBeingUsed(false);
			touched = false;
		}
		
		if(slider.rect.x > sliderBar.rect.x + sliderBar.rect.width - MaxX){
			slider.rect.x = sliderBar.rect.x + sliderBar.rect.width - MaxX;
			Gdx.app.log("[Debug]", "Too far.");
		} else if(slider.rect.x < sliderBar.rect.x + MinX){
			slider.rect.x = sliderBar.rect.x + MinX;
		}
		batch.draw(Assets.getFilteredTexture("SliderBarIcon"), slider.rect.x, slider.rect.y);
		
		double newSliderPos = slider.rect.x - sliderBar.rect.x - MinX;
		sliderPercentage = 100/(sliderRange/newSliderPos);
	}

}
