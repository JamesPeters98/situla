package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.screens.Options;

public class Input {
	
	private static boolean toggleRes;
	private static GameScreen gameScreen;
	private static double speed = 1500;
	private static boolean otherInputBeingUsed;

	public static void check(Game game, GameScreen screen){
		gameScreen = screen;
		keyPress(game);
		accelerometer(game);
		checkResToggle(game);
	}
	
	public static void check(Game game){
		keyPress(game);
		accelerometer(game);
		checkResToggle(game);
	}
	
	
	private static void keyPress(Game game){
		if(Gdx.input.isKeyPressed(Keys.S)) gameScreen.setScore(+100);
		if(Gdx.input.isKeyPressed(Keys.E)) gameScreen.setGameState(gameScreen.getGameOver());
		if(Gdx.input.isKeyPressed(Keys.SPACE)) speed = Options.getSensitivity()*50;
		if(!Gdx.input.isKeyPressed(Keys.SPACE) && !Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) speed = Options.getSensitivity()*30;
		if(Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) speed = Options.getSensitivity()*10;
		if(Gdx.input.isKeyPressed(Keys.A)) gameScreen.bucket.addPos((float) - speed * Gdx.graphics.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.D)) gameScreen.bucket.addPos((float) + speed * Gdx.graphics.getDeltaTime());
		if(!Gdx.input.isKeyPressed(Keys.ESCAPE)) {
		}
	}
	
	private static void accelerometer(Game game){
		if(Gdx.input.getAccelerometerY() <= 10 && Gdx.input.getAccelerometerY() >= 0.05f) gameScreen.bucket.addPos((float) + (Gdx.input.getAccelerometerY() * Options.getSensitivity() * 7) * Gdx.graphics.getDeltaTime());
		if(Gdx.input.getAccelerometerY() <= -0.05f && Gdx.input.getAccelerometerY() >= -10) gameScreen.bucket.addPos((float) + (Gdx.input.getAccelerometerY() * Options.getSensitivity() * 7) * Gdx.graphics.getDeltaTime());
		//if(Gdx.input.getAccelerometerY() < 0.5f && Gdx.input.getAccelerometerY() > 0.5f);
		
		//		else if(Gdx.input.getAccelerometerY() > 7) GameScreen.bucket.x += (7 * 200) * Gdx.graphics.getDeltaTime();
//		else if(Gdx.input.getAccelerometerY() < -7) GameScreen.bucket.x += (-7 * 200) * Gdx.graphics.getDeltaTime();
	}
	
	public static boolean checkIfTouchedInRectangle(Image image, Camera camera){
		Vector3 touchPoint = new Vector3();
		camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		return TouchTester.rectangle(image.rect, touchPoint.x, touchPoint.y);
	}
	
	public static boolean checkIfTouchedInRectangle(Rectangle rect, Camera camera){
		Vector3 touchPoint = new Vector3();
		camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		return TouchTester.rectangle(rect, touchPoint.x, touchPoint.y);
	}
	
	public static void checkResToggle(Game game){
		if(Gdx.input.isKeyPressed(Keys.R)){
			if(toggleRes == false){
				Resize.set1080(game, true);
				toggleRes = true;
			} else {
				Resize.set720(game, false);
				toggleRes = false;
			}
			
		}
	}
	
	public static boolean isEscape(){
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isOtherInputBeingUsed() {
		return otherInputBeingUsed;
	}

	public static void setOtherInputBeingUsed(boolean otherInputBeingUsed) {
		Input.otherInputBeingUsed = otherInputBeingUsed;
	}
	
}
