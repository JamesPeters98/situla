package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.JsonValue;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;

public class LevelSelectButton {
	
	int visableId;
	String uniqueId;
	int intUniqueId;
	GameScreen gamescreen;
	Button button;
	Preferences pref;
	boolean first;
	
	public LevelSelectButton(boolean first, int uniqueId, int visableId, GameScreen gamescreen){
		this.gamescreen = gamescreen;
		this.visableId = visableId;
		this.uniqueId = Integer.toString(uniqueId);
		this.intUniqueId = uniqueId;
		this.first = first;
		button = new Button();
		System.out.println(this.uniqueId);
		//pref = Gdx.app.getPreferences("Levels");
	}
	
	public float getLives(){
		if(GlobalScores.getInstance().levelLives.get(uniqueId) == null){
			return 0;
		} else {
			return GlobalScores.getInstance().levelLives.get(uniqueId);
			
		}
		//return pref.getInteger(Integer.toString(uniqueId));
	}
	
	public boolean isUnlocked(){
		if(first) return true;
		else if(GlobalScores.getInstance().levelLives.get(Integer.toString(intUniqueId-1)) != null) return true;
		else return false;
	}
	
	public void setLives(int lives){
		GlobalScores.getInstance().levelLives.put(uniqueId, lives);
//		pref.putInteger(Integer.toString(uniqueId), lives);
	}
	
	public int getVisableId(){
		return visableId;
	}
	
	public void update(){
//		Gdx.app.log("SITULA", "Game Finished");
//		if(gamescreen.getGameState() == gamescreen.GAME_FINISHED){
//			if(gamescreen.getLives() > getLives()){
//				
//				setLives(gamescreen.getLives());
//				gamescreen.SaveGame();
////				pref.flush();
//			}
//		}
//		if(getLives() == 0){
//			
//		}
	}
	
	public void render(Game game, OrthographicCamera camera, SpriteBatch batch, float x, float y, float height, float width, int id){
		update();
		batch.draw(Assets.getFilteredTexture("ButtonLevelSelect"), x, y);
		if(getLives() == 0 && isUnlocked()) Assets.font24.draw(batch, "Play", x+60, y+ 44);
		else if(getLives() == 0 && !isUnlocked()) Assets.font24.draw(batch, "Locked", x+36, y+ 44);
		if(getLives() >= 1 && isUnlocked()) batch.draw(Assets.getFilteredTexture("LifeDropIconSelector"), x+33, y+8);
		if(getLives() >= 2 && isUnlocked()) batch.draw(Assets.getFilteredTexture("LifeDropIconSelector"), x+70, y+8);
		if(getLives() >= 3 && isUnlocked()) batch.draw(Assets.getFilteredTexture("LifeDropIconSelector"), x+107, y+8);
		float fontX = (x + (width/2)) - ((Assets.font72.getBounds(Integer.toString(id)).width)/2);
		float fontY = (y + (height/2)) + ((Assets.font72.getBounds(Integer.toString(id)).height)/2) + 10;
		Assets.font72.draw(batch, Integer.toString(id), fontX, fontY);
		Rectangle rect = new Rectangle(x, y, width, height);
		if(button.createRectangleButton(rect, camera, batch, game)&&isUnlocked()){
			gamescreen.reset();
			game.setScreen(gamescreen);
		}
	}

}
