package com.jameslfc19.situla.utils;

import com.badlogic.gdx.math.Rectangle;

public class TouchTester {
	
	public static boolean rectangle(Rectangle rect, int x, int y){
		if(x >= rect.x && x <= rect.x + rect.width && y >= rect.y && y <= rect.y + rect.height){
			return true;
		}
		return false;
		
	}

	public static boolean rectangle(Rectangle rect, float x, float y) {
		if(x >= rect.x && x <= rect.x + rect.width && y >= rect.y && y <= rect.y + rect.height){
			return true;
		}
		return false;
	}
}
