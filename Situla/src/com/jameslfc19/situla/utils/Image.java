package com.jameslfc19.situla.utils;

import com.badlogic.gdx.math.Rectangle;

public class Image {
	
	public Rectangle rect;

	public Image(float x, float y, float height, float width){
		rect = new Rectangle();
		rect.x = x;
		rect.y = y;
		rect.height = height;
		rect.width = width;
	}
}
