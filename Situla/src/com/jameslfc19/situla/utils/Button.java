package com.jameslfc19.situla.utils;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.assets.Assets;

public class Button {
	
	private boolean hasPressed = false;

	public boolean createTextButton(Image button, OrthographicCamera camera, SpriteBatch batch, Game game, BitmapFont font, String buttonText, boolean onRight){
		Texture buttonPressedTexture;
		Texture buttonTexture;
		float textOffset;
		float textIndent = 175;
		float textX;
		if(onRight){
			buttonPressedTexture = Assets.getFilteredTexture("ButtonRightPressed");
			buttonTexture = Assets.getFilteredTexture("ButtonRight");
			textOffset = -80;
			textX = button.rect.x + textIndent;
		} else {
			buttonPressedTexture = Assets.getFilteredTexture("ButtonPressed");
			buttonTexture = Assets.getFilteredTexture("Button");
			textOffset = 80;
			textX = button.rect.x + button.rect.width - (Assets.font60.getBounds(buttonText).width) - textIndent;
		}
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
			Input.setOtherInputBeingUsed(true);
		} 
		if(Input.checkIfTouchedInRectangle(button, camera) && hasPressed){
			batch.draw(buttonPressedTexture, button.rect.x, button.rect.y);
			font.draw(batch, buttonText,textX + textOffset, (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2)+10);
		} 
		if(!Input.checkIfTouchedInRectangle(button, camera) || hasPressed == false) {
			batch.draw(buttonTexture, button.rect.x, button.rect.y);
			font.draw(batch, buttonText, textX, (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2)+10);
		}	
		
		if(!Gdx.input.isTouched() && Input.checkIfTouchedInRectangle(button, camera) && hasPressed == true){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
			return true;
		} else if(!Gdx.input.isTouched()){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
		}
		return false;
	}
	
	public boolean createTextButtonSquare(Image button, OrthographicCamera camera, SpriteBatch batch, Game game, BitmapFont font, String buttonText){
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
			Input.setOtherInputBeingUsed(true);
		} 
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && hasPressed){
			batch.draw(Assets.getFilteredTexture("ButtonSquarePressed"), button.rect.x, button.rect.y);
			font.draw(batch, buttonText, (button.rect.x + ((button.rect.width) /2)) - (Assets.font60.getBounds(buttonText).width/2)-5, (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2)-3);
		} else {
			batch.draw(Assets.getFilteredTexture("ButtonSquare"), button.rect.x, button.rect.y);
			font.draw(batch, buttonText, (button.rect.x + ((button.rect.width) /2)) - (Assets.font60.getBounds(buttonText).width/2), (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2));
		}	
		
		if(!Gdx.input.isTouched() && hasPressed == true){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean createToggleButton(Image button, OrthographicCamera camera, SpriteBatch batch, Game game, BitmapFont font, String buttonTextFalse, String buttonTextTrue, boolean toggle){
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && toggle && !hasPressed && !Input.isOtherInputBeingUsed()){
			toggle = false;
			hasPressed = true;
			Input.setOtherInputBeingUsed(true);
		} else if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !toggle && !hasPressed && !Input.isOtherInputBeingUsed()) {
			toggle = true;
			hasPressed = true;
			Input.setOtherInputBeingUsed(true);
		}	
		
		if(toggle){
			batch.draw(Assets.getFilteredTexture("ButtonCenteredPressed"), button.rect.x, button.rect.y);
			font.draw(batch, buttonTextTrue, (button.rect.x + ((button.rect.width) /2)) - (Assets.font60.getBounds(buttonTextTrue).width/2)-5, (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonTextTrue).height/2)-3);
		} else {
			batch.draw(Assets.getFilteredTexture("ButtonCentered"), button.rect.x, button.rect.y);
			font.draw(batch, buttonTextFalse, (button.rect.x + ((button.rect.width) /2)) - (Assets.font60.getBounds(buttonTextFalse).width/2), (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonTextFalse).height/2));
		}
		
		if(!Gdx.input.isTouched() && hasPressed == true){
			hasPressed = false;
			Input.setOtherInputBeingUsed(false);
		}
		
		return toggle;
	}
	
	public boolean createSetScreenButton(Image button, OrthographicCamera camera, SpriteBatch batch, Game game, BitmapFont font, String buttonText, Screen screen, boolean onRight){
		Texture buttonPressedTexture;
		Texture buttonTexture;
		float textOffset;
		float textIndent = 175;
		float textX;
		if(onRight){
			buttonPressedTexture = Assets.getFilteredTexture("ButtonRightPressed");
			buttonTexture = Assets.getFilteredTexture("ButtonRight");
			textOffset = -80;
			textX = button.rect.x + textIndent;
		} else {
			buttonPressedTexture = Assets.getFilteredTexture("ButtonPressed");
			buttonTexture = Assets.getFilteredTexture("Button");
			textOffset = 80;
			textX = button.rect.x + button.rect.width - (Assets.font60.getBounds(buttonText).width) - textIndent;
		}
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
			Input.setOtherInputBeingUsed(true);
		} 
		
		if(Input.checkIfTouchedInRectangle(button, camera) && hasPressed){
			batch.draw(buttonPressedTexture, button.rect.x, button.rect.y);
			font.draw(batch, buttonText, textX + textOffset, (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2)+10);
		} 
		if(!Input.checkIfTouchedInRectangle(button, camera) || !hasPressed) {
			batch.draw(buttonTexture, button.rect.x, button.rect.y);
			font.draw(batch, buttonText, textX , (button.rect.y + ((button.rect.height) /2)) + (Assets.font60.getBounds(buttonText).height/2)+10);
		}	
		
		if(!Gdx.input.isTouched() && Input.checkIfTouchedInRectangle(button, camera) && hasPressed){
			Input.setOtherInputBeingUsed(false);
			game.setScreen(screen);
			hasPressed = false;
			return true;
		} else if(!Gdx.input.isTouched()){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
		} 
		
		return false;
		
	}
	
	public boolean createIconButton(Image button, Texture buttonNotPressed, Texture buttonPressed, OrthographicCamera camera, SpriteBatch batch, Game game){
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
		} 
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && hasPressed){
			batch.draw(buttonPressed, button.rect.x, button.rect.y);
		} else {
			batch.draw(buttonNotPressed, button.rect.x, button.rect.y);
		}	
		
		if(!Gdx.input.isTouched() && hasPressed == true){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean createRectangleButton(Rectangle rect, OrthographicCamera camera, SpriteBatch batch, Game game){
		if(Input.checkIfTouchedInRectangle(rect, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
		} 
			
		if(!Gdx.input.isTouched() && hasPressed == true){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
			return true;
		} else {
			return false;
		}
	}
	
	public void createGooglePlusButton(Image button, Texture buttonDisabled, Texture buttonNormal, Texture buttonPressed, OrthographicCamera camera, SpriteBatch batch, Game game, boolean isLoggedIn){
		if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && !Input.isOtherInputBeingUsed()){
			hasPressed = true;
		} 
		if(isLoggedIn){
			batch.draw(buttonDisabled, button.rect.x, button.rect.y);
			Assets.font36.draw(batch, "SignOut", button.rect.x + 160, button.rect.y + 100);
		} else if(Input.checkIfTouchedInRectangle(button, camera) && Gdx.input.isTouched() && hasPressed){
			batch.draw(buttonPressed, button.rect.x, button.rect.y);
		} else {
			batch.draw(buttonNormal, button.rect.x, button.rect.y);
		}	
		
		if(!Gdx.input.isTouched() && hasPressed == true){
			Input.setOtherInputBeingUsed(false);
			hasPressed = false;
			if(isLoggedIn){
				Situla.GooglePlay.userSignOut();
				System.out.println("sign out");
			} else if (!isLoggedIn){
				Situla.GooglePlay.userSignIn();
			}
		} 
	}
}
