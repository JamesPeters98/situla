package com.jameslfc19.situla.assets;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;
import com.badlogic.gdx.utils.JsonWriter.OutputType;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.LevelData;
import com.jameslfc19.situla.screens.Options;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Files {
		
	//Write to file relative to /Bucket/ Directory on the OS
	public static void writeToFile(String fileName, String fileContent, boolean noOverwrite){			
		if (getOs() == "Windows"){
			FileHandle file = Gdx.files.external("/AppData/Local/Bucket/" + fileName);
			encodeFile(file, fileContent, noOverwrite);
		} else if (getOs() == "Mac"){
			FileHandle file = Gdx.files.external("/Library/Application Support/Bucket/" + fileName);
			encodeFile(file, fileContent, noOverwrite);		
		} else if (getOs() == "Linux" && Gdx.app.getType() == ApplicationType.Desktop){
			FileHandle file = Gdx.files.external("/.Bucket/" + fileName);
			encodeFile(file, fileContent, noOverwrite);
		} else if (Gdx.files.isExternalStorageAvailable()){		
			FileHandle file = Gdx.files.external("/.Bucket/" + fileName);
			encodeFile(file, fileContent, noOverwrite);
		} else {
			FileHandle file = Gdx.files.local("/.Bucket/" + fileName);
			encodeFile(file, fileContent, noOverwrite);
		}
	}
	
	//Write to file relative to /Bucket/ Directory on the OS
	public static String readFromFile(String fileName){			
		if (getOs() == "Windows"){
			FileHandle file = Gdx.files.external("/AppData/Local/Bucket/" + fileName);
			return decodeFile(file);
		} else if (getOs() == "Mac"){
			FileHandle file = Gdx.files.external("/Library/Application Support/Bucket/" + fileName);
			return decodeFile(file);	
		} else if (getOs() == "Linux" && Gdx.app.getType() == ApplicationType.Desktop){
			FileHandle file = Gdx.files.external("/.Bucket/" + fileName);
			return decodeFile(file);
		} else if(Gdx.files.isExternalStorageAvailable()){
			FileHandle file = Gdx.files.external("/.Bucket/" + fileName);
			return decodeFile(file);
		} else {
			FileHandle file = Gdx.files.local("/.Bucket/" + fileName);
			return decodeFile(file);
		}
	}
	
	public static String decodeFile(FileHandle file){
		String encodedFile = file.readString();
		System.out.println(encodedFile);
		if(!isValidBase64(encodedFile)) return "{}";
		String decodedFile = Base64Coder.decodeString(encodedFile);
		return decodedFile;
	}
	
	public static void encodeFile(FileHandle file, String fileContent, boolean noOverwrite){
		String encodedFile = Base64Coder.encodeString(fileContent);
		
		file.writeString(encodedFile, noOverwrite);
		
		
	}
	
	public static boolean isValidBase64(String file){
		char[] chars = file.toCharArray();
		if(chars.length % 4 != 0) return false;
		else return true;
	}
	
	public static String getOs(){
		if(System.getProperty("os.name").startsWith("Windows")){
			return "Windows";
		} else if (System.getProperty("os.name").startsWith("Mac")){
			return "Mac";
		} else if (System.getProperty("os.name").startsWith("Linux")){
			return "Linux";
		} else {
			return "Other";
		}
	}
	
	public static boolean doesFileExist(String fileName){
		if (getOs() == "Windows"){
			return Gdx.files.external("/AppData/Local/Bucket/" + fileName).exists();
		} else if (getOs() == "Mac"){
			return Gdx.files.external("/Library/Application Support/Bucket/" + fileName).exists();	
		} else if (getOs() == "Linux" && Gdx.app.getType() == ApplicationType.Desktop){
			return Gdx.files.external("/.Bucket/" + fileName).exists();
		} else if(Gdx.files.isExternalStorageAvailable()){
			return Gdx.files.external("/.Bucket/" + fileName).exists();	
		} else {
			return Gdx.files.local("/.Bucket/" + fileName).exists();
		}
		
	}
	
	public static void generatePreferenceJson() throws IOException{
		if (getOs() == "Windows"){
			FileHandle fileHandle = Gdx.files.external("/AppData/Local/Bucket/Preferences.json");
			StringWriter jsonText = new StringWriter();
			JsonWriter writer = new JsonWriter(jsonText);
			writer.object()
				.set("Music", true)
				.set("Sensitivity", 50)
			.pop();
			writer.close();
			fileHandle.writeString(jsonText.toString(), false);
			
		} else if (getOs() == "Mac"){
			FileHandle fileHandle = Gdx.files.external("/Library/Application Support/Bucket/Preferences.json");	
			StringWriter jsonText = new StringWriter();
			JsonWriter writer = new JsonWriter(jsonText);
			writer.object()
				.set("Music", true)
				.set("Sensitivity", 50)
			.pop();
			writer.close();
			fileHandle.writeString(jsonText.toString(), false);
		} else if (getOs() == "Linux"){
			FileHandle fileHandle = Gdx.files.external("/.Bucket/Preferences.json");
			StringWriter jsonText = new StringWriter();
			JsonWriter writer = new JsonWriter(jsonText);
			writer.object()
				.set("Music", true)
				.set("Sensitivity", 50)
			.pop();
			writer.close();
			fileHandle.writeString(jsonText.toString(), false);
		} else {
			FileHandle fileHandle = Gdx.files.external("/Bucket/Preferences.json");
			StringWriter jsonText = new StringWriter();
			JsonWriter writer = new JsonWriter(jsonText);
			writer.object()
				.set("Music", true)
				.set("Sensitivity", 50)
			.pop();
			writer.close();
			fileHandle.writeString(jsonText.toString(), false);
		}
	}
		
		public static void readPreferenceJson() throws IOException{
			if (getOs() == "Windows"){
				FileHandle fileHandle = Gdx.files.external("/AppData/Local/Bucket/Preferences.json");
				JsonReader reader = new JsonReader();
				JsonValue root = reader.parse(fileHandle);
				Options.setMusicToggle(root.getBoolean("Music"));
				Options.setSensitivity(root.getDouble("Sensitivity"));
				
			} else if (getOs() == "Mac"){
				FileHandle fileHandle = Gdx.files.external("/Library/Application Support/Bucket/Preferences.json");	
				JsonReader reader = new JsonReader();
				JsonValue root = reader.parse(fileHandle);
				Options.setMusicToggle(root.getBoolean("Music"));
				Options.setSensitivity(root.getDouble("Sensitivity"));
			} else if (getOs() == "Linux"){
				FileHandle fileHandle = Gdx.files.external("/.Bucket/Preferences.json");
				JsonReader reader = new JsonReader();
				JsonValue root = reader.parse(fileHandle);
				Options.setMusicToggle(root.getBoolean("Music"));
				Options.setSensitivity(root.getDouble("Sensitivity"));
			} else {
				FileHandle fileHandle = Gdx.files.external("/Bucket/Preferences.json");
				JsonReader reader = new JsonReader();
				JsonValue root = reader.parse(fileHandle);
				Options.setMusicToggle(root.getBoolean("Music"));
				Options.setSensitivity(root.getDouble("Sensitivity"));
			}
	}
		
		public static void writePreferenceJson() throws IOException{
			if (getOs() == "Windows"){
				FileHandle fileHandle = Gdx.files.external("/AppData/Local/Bucket/Preferences.json");
				StringWriter jsonText = new StringWriter();
				JsonWriter writer = new JsonWriter(jsonText);
				writer.object()
					.set("Music", Options.isMusicToggle())
					.set("Sensitivity", Options.getSensitivity())
				.pop();
				writer.close();
				fileHandle.writeString(jsonText.toString(), false);
				
			} else if (getOs() == "Mac"){
				FileHandle fileHandle = Gdx.files.external("/Library/Application Support/Bucket/Preferences.json");	
				StringWriter jsonText = new StringWriter();
				JsonWriter writer = new JsonWriter(jsonText);
				writer.object()
					.set("Music", Options.isMusicToggle())
					.set("Sensitivity", Options.getSensitivity())
				.pop();
				writer.close();
				fileHandle.writeString(jsonText.toString(), false);
			} else if (getOs() == "Linux"){
				FileHandle fileHandle = Gdx.files.external("/.Bucket/Preferences.json");
				StringWriter jsonText = new StringWriter();
				JsonWriter writer = new JsonWriter(jsonText);
				writer.object()
					.set("Music", Options.isMusicToggle())
					.set("Sensitivity", Options.getSensitivity())
				.pop();
				writer.close();
				fileHandle.writeString(jsonText.toString(), false);
			} else {
				FileHandle fileHandle = Gdx.files.external("/Bucket/Preferences.json");
				StringWriter jsonText = new StringWriter();
				JsonWriter writer = new JsonWriter(jsonText);
				writer.object()
					.set("Music", Options.isMusicToggle())
					.set("Sensitivity", Options.getSensitivity())
				.pop();
				writer.close();
				fileHandle.writeString(jsonText.toString(), false);
			}
		}
		
		public static void writeLevelDataJson() throws IOException{
			if (getOs() == "Windows"){
				FileHandle fileHandle = Gdx.files.external("/AppData/Local/Bucket/LevelData.dat");				
				Json json = new Json();

				String txt = json.toJson(GlobalScores.getInstance());
				fileHandle.writeString(txt, false);
				encodeFile(fileHandle, txt, false);
			} else if (getOs() == "Mac"){
				FileHandle fileHandle = Gdx.files.external("/Library/Application Support/Bucket/LevelData.dat");	
				Json json = new Json();

				String txt = json.toJson(GlobalScores.getInstance());
				fileHandle.writeString(txt, false);
				encodeFile(fileHandle, txt, false);
			} else if (getOs() == "Linux"){
				FileHandle fileHandle = Gdx.files.external("/.Bucket/LevelData.dat");
				Json json = new Json();

				String txt = json.toJson(GlobalScores.getInstance());
				fileHandle.writeString(txt, false);
				encodeFile(fileHandle, txt, false);
			} else {
				FileHandle fileHandle = Gdx.files.external("/Bucket/LevelData.dat");			
				Json json = new Json();

				String txt = json.toJson(GlobalScores.getInstance());
				fileHandle.writeString(txt, false);
				encodeFile(fileHandle, txt, false);
			}
		}
			
			public static void readLevelDataJson() throws IOException{
				if (getOs() == "Windows"){
					FileHandle fileHandle = Gdx.files.external("/AppData/Local/Bucket/LevelData.dat");
					StringWriter jsonText = new StringWriter();
					Json json = new Json();
					String file = decodeFile(fileHandle);
					GlobalScores.setInstace((json.fromJson(GlobalScores.class, file)));
				} else if (getOs() == "Mac"){
					FileHandle fileHandle = Gdx.files.external("/Library/Application Support/Bucket/LevelData.dat");	
					StringWriter jsonText = new StringWriter();
					Json json = new Json();
				
					String file = decodeFile(fileHandle);
					GlobalScores.setInstace((json.fromJson(GlobalScores.class, file)));
				} else if (getOs() == "Linux"){
					FileHandle fileHandle = Gdx.files.external("/.Bucket/LevelData.dat");
					StringWriter jsonText = new StringWriter();
					Json json = new Json();

					String file = decodeFile(fileHandle);
					GlobalScores.setInstace((json.fromJson(GlobalScores.class, file)));
				} else {
					FileHandle fileHandle = Gdx.files.external("/Bucket/LevelData.dat");
					StringWriter jsonText = new StringWriter();
					Json json = new Json();

					String file = decodeFile(fileHandle);
					GlobalScores.setInstace((json.fromJson(GlobalScores.class, file)));
				}
		}
			
			
	
	    //Generates file relative to /Bucket/ Directory on the OS
		//Use writeToFile() instead, it's almost exactly the same but has extra functionality and doesn't require this step.
		@Deprecated
		public static void generateFile(String fileName){			
			if (getOs() == "Windows"){
				if (doesFileExist("/AppData/Local/Bucket/" + fileName)){
					return;
				} else {
					FileHandle file = Gdx.files.external("/AppData/Local/Bucket/" + fileName);
					String encodedFile = Base64Coder.encodeString("");
					file.writeString(encodedFile, true);
				}
			} else if (getOs() == "Mac"){
				if (doesFileExist("/Library/Application Support/Bucket/" + fileName)){
					return;
				} else {
					FileHandle file = Gdx.files.external("/Library/Application Support/Bucket/" + fileName);
					String encodedFile = Base64Coder.encodeString("");
					file.writeString(encodedFile, true);		
				}
			} else if (getOs() == "Linux"){
				if (doesFileExist("/Bucket/" + fileName)){
					return;
				} else {
					FileHandle file = Gdx.files.external("/Bucket/" + fileName);
					String encodedFile = Base64Coder.encodeString("");
					file.writeString(encodedFile, true);		
				}
			} else {
				if (doesFileExist("/Bucket/" + fileName)){
					return;
				} else {
					FileHandle file = Gdx.files.external("/Bucket/" + fileName);
					String encodedFile = Base64Coder.encodeString("");
					file.writeString(encodedFile, true);		
				}
			}
			
		}
}
