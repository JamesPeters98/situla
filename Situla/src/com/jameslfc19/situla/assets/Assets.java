package com.jameslfc19.situla.assets;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.jameslfc19.situla.screens.Options;

public class Assets {
	
	public static AssetManager manager = new AssetManager();
	public static Sound drop;
	public static Music rain, schemingWeasel, sneakySnitch;
	public static BitmapFont font140, font72, font60, font50, font36, font24;
	private static HashMap<String,Music> Music = new HashMap<String, Music>();
	private static HashMap<String,String> Textures = new HashMap<String,String>();
	private static HashMap<String,String> FilteredTextures = new HashMap<String,String>();
	private static HashMap<String,Sprite> Sprites = new HashMap<String,Sprite>();
	private static TextureParameter filteredParameters = new TextureParameter();
	public static Texture Splash;
	public static TextureAtlas atlas;
	
	public static final String 
	BG_BLUE = "backgrounds/generalBlueBg",
	BUCKET_ICON = "icons/bucketIcon",
	NORMAL_DROPLET = "drops/dropSmall",
	LIFE_DROPLET = "drops/dropSmallLife",
	RUSTY_DROP = "drops/dropSmallBad",
	SLUDGE_DROP = "drops/dropSmallSludge",
	STANDARD_BUCKET_TOP = "buckets/standard/top",
	STANDARD_BUCKET_BACK = "buckets/standard/back",
	STANDARD_BUCKET_BACK_WIN = "buckets/standard/back_win",
	STANDARD_BUCKET_BACK_FAIL = "buckets/standard/back_fail",
	ROOM1 = "rooms/IndoorRoom/room",
	ROOM1_LEFT = "rooms/IndoorRoom/room_left",
	ROOM1_RIGHT = "rooms/IndoorRoom/room_right",
	ROOM1_THUMBNAIL = "rooms/IndoorRoom/roomThumbnail",
	ROOM1_THUMBNAIL_PRESSED = "rooms/IndoorRoom/roomThumbnailPressed",
	WINDLEVEL = "rooms/IndoorRoom/room",
	WINDLEVEL_LEFT = "rooms/IndoorRoom/room_left",
	WINDLEVEL_RIGHT = "rooms/IndoorRoom/room_right",
	WINDLEVEL_THUMBNAIL = "rooms/IndoorRoom/roomThumbnail",
	WINDLEVEL_THUMBNAIL_PRESSED = "rooms/IndoorRoom/roomThumbnailPressed"
	;

	public static void load(){
		Splash =  new Texture(Gdx.files.internal("data/res/splashScreen.png"));
		Splash.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		atlas = new TextureAtlas(Gdx.files.internal("images/situla.atlas"));
		
		Texture.setEnforcePotImages(false);
		filteredParameters.minFilter = TextureFilter.Linear;
		filteredParameters.magFilter = TextureFilter.Linear;
		filteredParameters.genMipMaps = true;
		
		//addSprite(BG_BLUE, "backgrounds/generalBlueBg");
		
		//addTextureWithFilter("BackgroundBlue", "data/res/backgrounds/generalBlueBg.png");
//		addTextureWithFilter("StandardBucketTop", "data/res/buckets/standard/top.png");
//		addTextureWithFilter("StandardBucketBack", "data/res/buckets/standard/back.png");
//		addTextureWithFilter("StandardBucketBackWin", "data/res/buckets/standard/back_win.png");
//		addTextureWithFilter("StandardBucketBackFail", "data/res/buckets/standard/back_fail.png");
//		addTextureWithFilter("DropletImage", "data/res/drops/dropSmall.png");
//		addTextureWithFilter("RustyDropletImage", "data/res/drops/dropSmallBad.png");
//		addTextureWithFilter("LifeDropletImage", "data/res/drops/dropSmallLife.png");
//		addTextureWithFilter("SludgeDropletImage", "data/res/drops/dropSmallSludge.png");
//		addTextureWithFilter("Room1", "data/res/rooms/IndoorRoom/room.png");
//		addTextureWithFilter("Room1_Left", "data/res/rooms/IndoorRoom/room_left.png");
//		addTextureWithFilter("Room1_Right", "data/res/rooms/IndoorRoom/room_right.png");
		addTextureWithFilter("Room1Thumbnail", "data/res/rooms/IndoorRoom/roomThumbnail.png");
		addTextureWithFilter("Room1ThumbnailPressed", "data/res/rooms/IndoorRoom/roomThumbnailPressed.png");
		addTextureWithFilter("WindLevel", "data/res/rooms/WindLevel/room.png");
		addTextureWithFilter("WindLevel_Left", "data/res/rooms/WindLevel/room_left.png");
		addTextureWithFilter("WindLevel_Right", "data/res/rooms/WindLevel/room_right.png");
		addTextureWithFilter("WindLevelThumbnail", "data/res/rooms/WindLevel/roomThumbnail.png");
		addTextureWithFilter("WindLevelThumbnailPressed", "data/res/rooms/WindLevel/roomThumbnailPressed.png");
		addTextureWithFilter("Tap", "data/res/icons/tap.png");
		addTextureWithFilter("LifeDropIcon", "data/res/icons/lifeDrop.png");
		addTextureWithFilter("LifeDropIconSelector", "data/res/icons/selectorLifeDrop.png");
		addTextureWithFilter("Button", "data/res/buttons/button.png");
		addTextureWithFilter("ButtonPressed", "data/res/buttons/buttonPressed.png");
		addTextureWithFilter("ButtonRight", "data/res/buttons/buttonRight.png");
		addTextureWithFilter("ButtonRightPressed", "data/res/buttons/buttonRightPressed.png");
		addTextureWithFilter("ButtonLevelSelect", "data/res/buttons/levelSelectButton.png");
		//addTextureWithFilter("ButtonSquare", "data/res/buttons/buttonSquare.png");
		//addTextureWithFilter("ButtonSquarePressed", "data/res/buttons/buttonSquarePressed.png");
		addTextureWithFilter("ButtonCentered", "data/res/buttons/buttonCentered.png");
		addTextureWithFilter("ButtonCenteredPressed", "data/res/buttons/buttonCenteredPressed.png");
		addTextureWithFilter("PauseIcon", "data/res/icons/pause.png");
		addTextureWithFilter("PauseIconPressed", "data/res/icons/pausePressed.png");
		addTextureWithFilter("Logo", "data/res/logo/logo.png");
		addTextureWithFilter("SliderBarBackground", "data/res/buttons/slider/bar.png");
		addTextureWithFilter("SliderBarIcon", "data/res/buttons/slider/slider.png");
		addTextureWithFilter("TutorialOverlayBackground", "data/res/gui/tutorialOverlayBackground.png");
		addTextureWithFilter("TutorialOverlaySegment", "data/res/gui/tutorialOverlaySegment.png");
		//addTextureWithFilter("BucketIcon", "data/res/icons/bucketIcon.png");
		addTextureWithFilter("GooglePlusButtonDisabled", "data/res/buttons/btn_g+disabled.png");
		addTextureWithFilter("GooglePlusButtonNormal", "data/res/buttons/btn_g+normal.png");
		addTextureWithFilter("GooglePlusButtonPressed", "data/res/buttons/btn_g+pressed.png");
		
		drop = Gdx.audio.newSound(Gdx.files.internal("data/drop.wav"));
		rain = Gdx.audio.newMusic(Gdx.files.internal("data/res/music/rain.mp3"));
		schemingWeasel = Gdx.audio.newMusic(Gdx.files.internal("data/res/music/Scheming Weasel faster.mp3"));
		sneakySnitch = Gdx.audio.newMusic(Gdx.files.internal("data/res/music/Sneaky Snitch.mp3"));	
		
		rain.setLooping(true);
		rain.setVolume(0.5f);
		schemingWeasel.setLooping(true);
		sneakySnitch.setLooping(true);
				
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("data/fonts/Blokletters-Viltstift.ttf"));
		
		font24 = generator.generateFont(24);
		font36 = generator.generateFont(36);
		font50 = generator.generateFont(50);
		font60 = generator.generateFont(60);
		font72 = generator.generateFont(72);
		font140 = generator.generateFont(140);
		
		font50.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font24.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font36.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font140.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font60.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font72.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		generator.dispose();
		
	}
	
	public static boolean doneLoading(){
		if(manager.update()){
//			filterTextures();
			return true;
		}
		return false;
	}
	
	public static void loadOther(){
		
	}
	
	public static void playSound(Sound sound){
		sound.play();
	}

	public static void playMusic(String id, Music music) {
		if(Options.isMusicToggle()){
			Music.put(id, music);
			music.play();
		}
	}
	
	public static void stopSound(Sound sound){
		sound.stop();
	}

	public static void stopMusic(Music music) {
		music.stop();
	}
	
	public static void playAllMusic(){
		for(Music music : Music.values()){
			if(!music.isPlaying()){
				music.play();
			}
		}
	}
	
	public static void pauseAllMusic(){
		for(Music music : Music.values()){
			if(music.isPlaying()){
				music.pause();
			}
		}
	}
	
	public static void addTexture(String key, String url){
		manager.load(url, Texture.class);
		Textures.put(key, url);
	}
	
	public static void addTextureWithFilter(String key, String url){
		manager.load(url, Texture.class, filteredParameters);
		FilteredTextures.put(key, url);
	}
	
	public static void addSprite(String key, String url){
		Sprite sprite = atlas.createSprite(url);
		Sprites.put(key, sprite);
	}
	
	public static Texture getTexture(String key){
		String url = Textures.get(key);
		return manager.get(url, Texture.class);
	}
	
	public static Texture getFilteredTexture(String key){
		String url = FilteredTextures.get(key);
		return manager.get(url, Texture.class);
	}
	
	public static Sprite getSprite(String key, float x, float y){
		Sprite sprite = atlas.createSprite(key);
		sprite.setPosition(x, y);
		return sprite;
	}
	
	public static Sprite getSprite(String key){
		return getSprite(key,0,0);
	}
	
//	private static void filterTextures(){
//		Iterator<Entry<String, String>> iter = FilteredTextures.entrySet().iterator();
//		while(iter.hasNext()){
//			Entry<String, String> entry = iter.next();
//			String url = entry.getValue();
//			Texture tex = manager.get(url);
//			tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		}
//	}
	
}
