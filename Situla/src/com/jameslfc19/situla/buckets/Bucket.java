package com.jameslfc19.situla.buckets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;

public class Bucket {
	
	float x = 0;
	float y = 20;
	
	Sprite top, back, backWin, backFail;
	
	public Bucket(Sprite top, Sprite back, Sprite backWin, Sprite backFail){
		this.top = top;
		this.back = back;
		this.backWin = backWin;
		this.backFail = backFail;
	}
	
	public void update(SpriteBatch batch, int FaceState){
		top.setPosition(x, y);
		back.setPosition(x, y);
		backWin.setPosition(x, y);
		backFail.setPosition(x, y);
		
		top.draw(batch);
		if(FaceState == GameScreen.FACE_NORMAL)back.draw(batch);
		if(FaceState == GameScreen.FACE_WIN)backWin.draw(batch);
		if(FaceState == GameScreen.FACE_FAIL)backFail.draw(batch);
	}
	
	public void setPos(float x){
		this.x = x;
	}
	
	public void addPos(float x){
		this.x = this.x + x;
	}
	
	public float getWidth(){
		return top.getWidth();
	}
	
	public float getHeight(){
		return top.getHeight();
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}

}
