package com.jameslfc19.situla;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

public class GlobalScores {
	
	private static GlobalScores scores = new GlobalScores();
	
	private GlobalScores(){
		
	}
	
	public static GlobalScores getInstance(){
		return scores;
	}
	
	public static void setInstace(GlobalScores scores){
		GlobalScores.scores = scores;
		System.out.println(GlobalScores.getInstance().levelLives);
		System.out.println(GlobalScores.getInstance().levelLives.get("1"));
		System.out.println(GlobalScores.getInstance().EndlessHighscore);
	}
	
	
	public int EndlessHighscore = 0;
	public int Section1_Level1 = 0;
	public HashMap<String, Integer> levelLives = new HashMap<String, Integer>();
	
}
