package com.jameslfc19.situla.interfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.utils.Image;

public class TutorialItem {
	
	private Sprite sprite;
	private Rectangle bounds;
	private String description;
	
	public TutorialItem(Drop drop){
		sprite = drop.getSprite();
		bounds = drop.getRectangleBounds();
		description = drop.getDescription();
	}
	
	public TutorialItem(Sprite icon, Image image, String description){
		sprite = icon;
		this.bounds = image.rect;
		this.description = description;
	}
	
	public Sprite getSprite(){
		return sprite;
	}
	
	public String getDesrciption(){
		return description;
	}
	
	public Rectangle getBounds(){
		return bounds;
	}

}
