package com.jameslfc19.situla.interfaces;

public interface GooglePlayInterface {
	
	public String EndlessLeaderboard = "CgkI7aPrgYwCEAIQBg";
	public String JustGettingStarted_Ach = "CgkI7aPrgYwCEAIQAQ";
	public String GettingBetter_Ach = "CgkI7aPrgYwCEAIQAg";
	public String StillDoBetter_Ach = "CgkI7aPrgYwCEAIQCA";
	
	public void userSignIn();

	public void userSignOut();
	
	public void submitEndlessHighscore(long score);
	
	public void showEndlessLeaderboard();
	
	public void unlockAchievement(String id);
	
	public void showAchievements();
	
	public boolean isLoggedIn();
}
