package com.jameslfc19.situla.drops;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Image;


public class Normal extends Drop {
	
	float pairedY;
	
	public Normal() {
		super();
		score = 1;
		spawnChance = 1;
		drop = new Rectangle(0,0, 53, 70);
	}

	public Normal(Drop[] drops, int[] chances, int intervalAmount, int overallSpawnAmount) {
		super(drops, chances, intervalAmount, overallSpawnAmount);
		score = 1;
		spawnChance = 1;
		drop = new Rectangle(0,0, 53, 70);
	}
	
	public Normal(Drop[] drops, int[] chances, int overallSpawnAmount) {
		super(drops, chances, overallSpawnAmount);
		score = 1;
		spawnChance = 1;
		this.drop = new Rectangle(0,0, 53, 70);
	}
	
	public Normal(Drop drop, int overallSpawnAmount) {
		super(drop, overallSpawnAmount);
		score = 1;
		spawnChance = 1;
		this.drop = new Rectangle(0,0, 53, 70);
	}
	
	public Normal(Drop drop, int intervalAmount, int overallSpawnAmount) {
		super(drop, intervalAmount, overallSpawnAmount);
		score = 1;
		spawnChance = 1;
		this.drop = new Rectangle(0,0, 53, 70);
	}

	@Override
	public void landInBucket(GameScreen game, Iterator<Rectangle> iter) {
		Assets.playSound(Assets.drop);
		game.addToScore(score);
		game.setFaceState(GameScreen.FACE_WIN);
		iter.remove();
	}

	@Override
	public void hitBucketSide(GameScreen game, Iterator<Rectangle> iter) {
		game.setFaceState(GameScreen.FACE_FAIL);
		game.addToLives(-1);
		iter.remove();
	}

	@Override
	public void hitFloor(GameScreen game, Iterator<Rectangle> iter) {
		game.setFaceState(GameScreen.FACE_FAIL);
		game.addToLives(-1);
		iter.remove();
	}

	@Override
	public Sprite getSprite() {
		return Assets.getSprite(Assets.NORMAL_DROPLET);
	}
	
	@Override
	public float pairedX(){
		return 0;
	}
	
	@Override
	public float pairedY(){
		return 90;
	}
	
	@Override
	public int pairedChance(){
		return 5;
	}

	@Override
	public String getDescription() {
		return "Normal Drop: Miss this and you'll lose a life!";
	}

}
