package com.jameslfc19.situla.drops;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;

public class Sludge extends Drop {

	public Sludge() {
		super();
		lives = -1;
		spawnChance = 0;
		pixelsPerSecond = 260;
		drop = new Rectangle(0,0, 53, 70);
	}

	@Override
	public void landInBucket(GameScreen game, Iterator<Rectangle> iter) {
		game.RotateScreen();
		game.setFaceState(GameScreen.FACE_FAIL);
		game.addToLives(lives);
		iter.remove();
	}

	@Override
	public void hitBucketSide(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}

	@Override
	public void hitFloor(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}

	@Override
	public Sprite getSprite() {
		return Assets.getSprite(Assets.SLUDGE_DROP);
	}

	@Override
	public String getDescription() {
		return "Sludge Drop: Don't catch these or you'll want to hang yourself upside down!";
	}

}