package com.jameslfc19.situla.drops;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Image;

public class LifeDrop extends Drop {

	public LifeDrop() {
		super();
		score = 5;
		lives = 1;
		spawnChance = 30;
		pixelsPerSecond = 400;
		drop = new Rectangle(0,0, 53, 70);
	}

	@Override
	public void landInBucket(GameScreen game, Iterator<Rectangle> iter) {
		if(game.getLives() >= 3){
			game.addToScore(score);
		} else {
			game.addToLives(lives);
		}
		Assets.playSound(Assets.drop);
		iter.remove();
	}

	@Override
	public void hitBucketSide(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}

	@Override
	public void hitFloor(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}

	@Override
	public Sprite getSprite() {
		return Assets.getSprite(Assets.LIFE_DROPLET);
	}

	@Override
	public String getDescription() {
		return "Life Drop: Catch this and gain an extra life or 5 points if you already have 3!";
	}

}
