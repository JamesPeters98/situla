package com.jameslfc19.situla.drops;

import java.util.Iterator;

import com.badlogic.gdx.math.Rectangle;
import com.jameslfc19.situla.games.GameScreen;

public class NormalFast extends Normal{
	
	int spawnchance;

	public NormalFast() {
		super();
		spawnchance = 20;
		score = 3;
		lives = 0;
		pixelsPerSecond = 300;
	}
	
	public NormalFast(int spawnchance) {
		super();
		this.spawnchance = spawnchance;
		score = 3;
		lives = 0;
		pixelsPerSecond = 300;
	}
	
	@Override
	public int spawnChance() {
		return spawnchance;
	}
	
	@Override
	public Drop pairedDrop(){
		return null;
	}
	
	@Override
	public void hitBucketSide(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}

	@Override
	public void hitFloor(GameScreen game, Iterator<Rectangle> iter) {
		iter.remove();
	}
	
	@Override
	public String getDescription(){
		return "Disguised Drop: Watch out for these, they're there to catch you out! But you still get 3 points for catching 'em!";
	}

}
