package com.jameslfc19.situla.drops;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Image;

public abstract class Drop {
	
	//int id;
	int dropAmount;
	int score;
	int lives;
	int pixelsPerSecond = 200;
	int spawnChance;
	int pairedChance = 0;
	int pairedInterval;
	boolean usePairedInterval = false;
	//boolean usePair;
	Rectangle drop;
	public Drop[] chanceDrops;
	public int[] chances;
	int overallSpawnChance;
	
	public Drop(){
		//this.id = id;
		//this.usePair = usePair;
	}
	
	public Drop(Drop drop, int pairedChance){
		//this.id = id;
		//this.usePair = usePair;
		this.pairedInterval = 0;
		chanceDrops = new Drop[] {drop};
		chances = new int[] {pairedChance};
		this.overallSpawnChance = 100;
		this.pairedChance = pairedChance;
	}
	
	public Drop(Drop drop, int intervalAmount, int pairedChance){
		//this.id = id;
		//this.usePair = usePair;
		this.pairedInterval = intervalAmount;
		this.usePairedInterval = true;
		chanceDrops = new Drop[] {drop};
		chances = new int[] {pairedChance};
		this.overallSpawnChance = 100;
		this.pairedChance = pairedChance;
	}
	
	public Drop(Drop[] drops, int[] chances, int overallSpawnChance){
		//this.id = id;
		//this.usePair = usePair;
		this.pairedInterval = 0;
		this.usePairedInterval = true;
		this.overallSpawnChance = overallSpawnChance;
		this.chanceDrops = drops;
		this.chances = chances;
		checkValidPercentages();
	}
	
	public Drop(Drop[] drops, int[] chances, int pairedInterval, int overallSpawnChance){
		//this.id = id;
		//this.usePair = usePair;
		this.pairedInterval = pairedInterval;
		this.usePairedInterval = true;
		this.chanceDrops = drops;
		this.chances = chances;
		this.overallSpawnChance = overallSpawnChance;
		checkValidPercentages();
	}
	
	public Array<Rectangle> drops = new Array<Rectangle>();
	
	public void resetArray(){
		drops = new Array<Rectangle>();
	}
	
	private void checkValidPercentages(){
		int percentages = 0;
		for(int i = 0; i < chances.length; i++){
			percentages += chances[i];
		}
		if(percentages > 100)
			try {
				throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public abstract void landInBucket(GameScreen game, Iterator<Rectangle> iter);
	
	public abstract void hitBucketSide(GameScreen game, Iterator<Rectangle> iter);
	
	public abstract void hitFloor(GameScreen game, Iterator<Rectangle> iter);
	
	public int pixelsPerSecond(){
		return pixelsPerSecond;
	}
	
	// 1 to returned #
	public int spawnChance(){
		return spawnChance;
	}
	
	public abstract Sprite getSprite();
	public Rectangle getRectangleBounds(){
		return drop;
	}
	
	public Drop pairedDrop(){
		if(chanceDrops == null){
			Gdx.app.log("SITULA", "No chance drops.");
			return null;
		}
		if(dropAmount >= pairedInterval){
			Gdx.app.log("SITULA", "Drop amount valid.");
			if(MathUtils.random(1, 100) <= overallSpawnChance){
				Gdx.app.log("SITULA", "Spawn Chance valid.");
				int overallPercent = 0;
				for(int i = 0; i < chanceDrops.length; i++){
					if(MathUtils.random(1, 100) <= overallPercent + chances[i]){
						Gdx.app.log("SITULA", "Chance drop.");
						return chanceDrops[i];
					} else {
						overallPercent += chances[i];
					}
				}
			}
		}
		Gdx.app.log("SITULA", "Drop return null.");
		return null;
	}
	
	public float pairedX(){
		return 0;
	}
	
	public float pairedY(){
		return 0;
	}
	
	public int pairedChance(){
		return pairedChance;
	}
	
//	public int getId(){
//		return id;
//	}
	
	public void dropIncrement(){
		dropAmount ++;
	}
	
	public void resetDropAmount(){
		dropAmount = 0;
	}
	
	public int dropAmount(){
		return dropAmount;
	}
	
	public boolean isIncrementDrop(){
		return usePairedInterval;
	}
	
	public int intervalAmount(){
		return pairedInterval;
	}
	
	public abstract String getDescription();

}
