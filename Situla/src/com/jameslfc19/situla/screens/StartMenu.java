package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.EndlessGame;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.ImageCentered;
import com.jameslfc19.situla.utils.Input;

public class StartMenu implements Screen {
	
	Game game;
	Screen endlessgame, enviromentselect, optionsmenu;
	OrthographicCamera camera;
	SpriteBatch batch;
	Image play, campaign, logo, options, quit, gplus, leaderboards, achievements;
	Sprite background, background_left, background_right;
	Button endlessButton, campaignButton, optionsButton, quitButton, gplusButton, leaderboardButton, achievementsButton;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	public StartMenu(Game game){
		this.game = game;		
		endlessButton = new Button();
		campaignButton = new Button();
		optionsButton = new Button();
		quitButton = new Button();
		gplusButton = new Button();
		leaderboardButton = new Button();
		achievementsButton = new Button();
				
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		initImages();
			
		endlessgame = new EndlessGame(game);
		enviromentselect = new EnviromentSelect(game);
		optionsmenu = new Options(game, this);
	}
	
	public void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		play = new Image(0, 550, 141, 650);
		campaign = new Image(0, 370, 141, 650);
		achievements = new Image(0, 190, 141, 650);
		logo = new ImageCentered().Y(getViewportHeight() - 375, 250, 1200, viewportWidth);
		leaderboards = new Image(viewportWidth-672, 550, 141, 650);
		options = new Image(viewportWidth-672, 370, 141, 650);
		quit = new Image(viewportWidth-672, 190, 141, 650);
		gplus = new Image(20, 20, 144, 144);
	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.671875f, 0.921875f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		batch.draw(Assets.getFilteredTexture("Logo"), logo.rect.x, logo.rect.y);
		endlessButton.createSetScreenButton(play, camera, batch, game, Assets.font60, "Endless", endlessgame, false);
		campaignButton.createSetScreenButton(campaign, camera, batch, game, Assets.font60, "Campaign", enviromentselect, false);
		optionsButton.createSetScreenButton(options, camera, batch, game, Assets.font60, "Options", optionsmenu, true);
		if(quitButton.createTextButton(quit, camera, batch, game, Assets.font60, "Quit", true)) Gdx.app.exit();
		if(leaderboardButton.createTextButton(leaderboards, camera, batch, game, Assets.font60, "Highscores", true)) Situla.GooglePlay.showEndlessLeaderboard();
		if(achievementsButton.createTextButton(achievements, camera, batch, game, Assets.font60, "Achievements", false)) Situla.GooglePlay.showAchievements();
		gplusButton.createGooglePlusButton(gplus, Assets.getFilteredTexture("GooglePlusButtonDisabled"), Assets.getFilteredTexture("GooglePlusButtonNormal"), Assets.getFilteredTexture("GooglePlusButtonPressed"), camera, batch, game, Situla.GooglePlay.isLoggedIn());
		batch.end();
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		if(!Assets.schemingWeasel.isPlaying()){
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		StartMenu.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		StartMenu.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}
	
	public Screen toScreen(){
		return this;
	}

}
