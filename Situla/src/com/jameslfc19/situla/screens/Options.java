package com.jameslfc19.situla.screens;

import java.io.IOException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.assets.Files;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.Input;
import com.jameslfc19.situla.utils.Slider;

public class Options implements Screen {
	
	Game game;
	OrthographicCamera camera;
	SpriteBatch batch;
	Image button, quitbutton, sliderBar, sliderImg;
	Sprite background, background_left, background_right;
	Button music, back;
	Screen prevScreen;
	Slider slider;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	private static boolean musicToggle;
	private static double sensitivity;
	
	public Options(Game game, Screen prevScreen){
		this.game = game;
		this.prevScreen = prevScreen;
		
		music = new Button();
		back = new Button();
		slider = new Slider();
				
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		
		initImages();
	}
	
	private void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		button = new Image(getViewportWidth()/2 + 100, 550, 139, 139);
		quitbutton = new Image(viewportWidth-672, 220, 141, 650);
		sliderBar = new Image(getViewportWidth()/2 + 100, 450, 56, 413);
		sliderImg = new Image(getViewportWidth()/2 + 100 + (sliderBar.rect.width/2), 450, 23, 69);
	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.7f, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		Assets.font60.draw(batch, "Music", getViewportWidth()/2 - Assets.font60.getBounds("Music").width - 50, 650);
		Assets.font60.draw(batch, "Sensitivity", getViewportWidth()/2 - Assets.font60.getBounds("Sensitivity").width - 50, sliderBar.rect.y + 50);
		Assets.font140.draw(batch, "Options", getViewportWidth()/2 - Assets.font140.getBounds("Options").width/2, getViewportHeight() - 150);
		setMusicToggle(music.createToggleButton(button, camera, batch, game, Assets.font60, "Off", "On", musicToggle));
		if(back.createSetScreenButton(quitbutton, camera, batch, game, Assets.font60, "Back", prevScreen, true)){
			try {
				Files.writePreferenceJson();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(sensitivity);
		slider.createSlider(sliderBar, sliderImg, 11, 30, batch, camera, sensitivity);
		setSensitivity(slider.getSliderPercentage());
		batch.end();
		
		if(isMusicToggle() == false){
			Assets.pauseAllMusic();
		} else if(!Assets.schemingWeasel.isPlaying()){
			Assets.schemingWeasel.play();
		}
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		Options.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		Options.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	public static boolean isMusicToggle() {
		return musicToggle;
	}

	public static void setMusicToggle(boolean musicToggle) {
		Options.musicToggle = musicToggle;
	}

	public static double getSensitivity() {
		return sensitivity;
	}

	public static void setSensitivity(double sensitivity) {
		Options.sensitivity = sensitivity;
	}

}
