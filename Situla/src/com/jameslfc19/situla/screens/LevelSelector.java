package com.jameslfc19.situla.screens;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.drops.Normal;
import com.jameslfc19.situla.drops.NormalFast;
import com.jameslfc19.situla.drops.Rusty;
import com.jameslfc19.situla.drops.Sludge;
import com.jameslfc19.situla.games.CampaignGame;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.interfaces.TutorialItem;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.ImageCentered;
import com.jameslfc19.situla.utils.Input;
import com.jameslfc19.situla.utils.LevelSelectButton;
import com.jameslfc19.situla.utils.Strings;

public class LevelSelector implements Screen {
	
	Game game;
	Screen gameWin;
	OrthographicCamera camera;
	SpriteBatch batch;
	Sprite background, background_left, background_right;
	Image room1Image, logo, levelSelectButton, quitbutton;
	Button back;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	ArrayList<LevelSelectButton> levelSelectButtons =  new ArrayList<LevelSelectButton>();
	
	
	public LevelSelector(Game game){
		this.game = game;						
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		back = new Button();

		initImages();
		initLevels();
	}
	
	private void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		levelSelectButton = new Image(getViewportWidth()/2 - 522, 500, 169, 169);
		quitbutton = new Image(viewportWidth-672, 100, 141, 650);
		room1Image = new Image(viewportWidth/2 - 550, 400, 453, 284);
		logo = new ImageCentered().Y(getViewportHeight() - 400, 250, 1200, viewportWidth);
	}
	
	public void initLevels(){
		//Level 1 - Normal Drops, 6 second initial, 0 increase, 6 minimum.
		Drop[] level1drops = {new Normal()};
		TutorialItem[] level1tutorialitems = {new TutorialItem(DropsRegistry.Normal), new TutorialItem(Assets.getSprite(Assets.BUCKET_ICON), new Image(0,0,90,72), Strings.BUCKET_DESCRIPTION)};
		GameScreen level1 = new CampaignGame(game, 5, 6f, 0f, 6f, level1drops, level1tutorialitems,1,this);
		LevelSelectButton level1button = new LevelSelectButton(true, 1, 1, level1);
		levelSelectButtons.add(0, level1button);
		
		//Level 2 - Normal Drops, 3 second initial, 0.2 increase, 2 minimum.
		Drop[] level2drops = {new Normal(new Rusty(), 5, 100)};
		TutorialItem[] level2tutorialitems = {new TutorialItem(DropsRegistry.Normal), new TutorialItem(new Rusty())};
		GameScreen level2 = new CampaignGame(game, 10, 3f, 0.2f, 2f, level2drops, level2tutorialitems,2,this);
		LevelSelectButton level2button = new LevelSelectButton(false, 2, 2, level2);
		levelSelectButtons.add(1, level2button);
		
		//Level 3 - Normal Drops, 3 second initial, 0.2 increase, 2 minimum.
		Drop[] level3drops = {new Normal(new Rusty(), 2, 100)};
		TutorialItem[] level3tutorialitems = {new TutorialItem(DropsRegistry.Normal), new TutorialItem(new Rusty())};
		GameScreen level3 = new CampaignGame(game, 15, 3f, 0.2f, 1.5f, level3drops, level3tutorialitems,3,this);
		LevelSelectButton level3button = new LevelSelectButton(false, 3, 3, level3);
		levelSelectButtons.add(2, level3button);
		
		//Level 4 - Normal Drops, 3 second initial, 0.2 increase, 2 minimum.
		Drop[] level4drops = {new Normal(new Rusty(), 2, 100)};
		TutorialItem[] level4tutorialitems = {new TutorialItem(DropsRegistry.Normal), new TutorialItem(new Rusty())};
		GameScreen level4 = new CampaignGame(game, 15, 3f, 0.2f, 1.5f, level4drops, level4tutorialitems,4,this);
		LevelSelectButton level4button = new LevelSelectButton(false, 4, 4, level4);
		levelSelectButtons.add(3, level4button);
		
		//Level 5
		Drop[] level5NormalPairs = {new Rusty(), new Sludge()}; int[] level5Chances = {33,66};
		Drop[] level5drops = {new Normal(level5NormalPairs, level5Chances, 100)};
		TutorialItem[] level5tutorialitems = {new TutorialItem(DropsRegistry.Normal),new TutorialItem(DropsRegistry.Rusty),new TutorialItem(DropsRegistry.SludgeDrop)};
		GameScreen level5 = new CampaignGame(game, 10, 3f, 0.1f, 2f, level5drops, level5tutorialitems,5,this);
		LevelSelectButton level5button = new LevelSelectButton(false, 5, 5, level5);
		levelSelectButtons.add(4, level5button);
		
		//Level 6 - Normal Drops, 5 second initial, 0.1 increase, 2 minimum.
		Drop[] level6NormalPairs = {new Rusty(), new Sludge()}; int[] level6Chances = {33,66};
		Drop[] level6drops = {new Normal(level6NormalPairs, level6Chances, 100)};
		TutorialItem[] level6tutorialitems = {new TutorialItem(DropsRegistry.Normal),new TutorialItem(DropsRegistry.Rusty),new TutorialItem(DropsRegistry.SludgeDrop)};
		GameScreen level6 = new CampaignGame(game, 20, 2f, 0.15f, 1.5f, level6drops, level6tutorialitems,6,this);
		LevelSelectButton level6button = new LevelSelectButton(false, 6, 6, level6);
		levelSelectButtons.add(5, level6button);
		
		//Level 7 - Normal Drops, 5 second initial, 0.1 increase, 2 minimum.
		Drop[] level7NormalPairs = {new Rusty(), new Sludge()}; int[] level7Chances = {33,66};
		Drop[] level7drops = {new Normal(level7NormalPairs, level7Chances, 100), new NormalFast(3)};
		TutorialItem[] level7tutorialitems = {new TutorialItem(DropsRegistry.Normal),new TutorialItem(DropsRegistry.Rusty),new TutorialItem(DropsRegistry.SludgeDrop),new TutorialItem(DropsRegistry.NormalFast)};
		GameScreen level7 = new CampaignGame(game, 10, 3f, 0.15f, 2f, level7drops, level7tutorialitems,6,this);
		LevelSelectButton level7button = new LevelSelectButton(false, 7, 7, level7);
		levelSelectButtons.add(6, level7button);

	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.7f, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		batch.draw(Assets.getFilteredTexture("Logo"), logo.rect.x, logo.rect.y);
		back.createSetScreenButton(quitbutton, camera, batch, game, Assets.font60, "Back", Screens.enviromentSelect, true);
		drawLevelButtons(batch);
		batch.end();
	}
	
	private void drawLevelButtons(SpriteBatch batch){
		Iterator<LevelSelectButton> iter = levelSelectButtons.iterator();
		for(int row = 0; row < 3; row++){
			for(int column = 0; column < 5; column++){
				if(!iter.hasNext()) break;
				LevelSelectButton button = iter.next();
				float x = (levelSelectButton.rect.x + ((levelSelectButton.rect.width + 50)*column));
				float y = (levelSelectButton.rect.y - ((levelSelectButton.rect.height+ 50)*row));
				button.render(game,camera,batch,x,y,levelSelectButton.rect.height,levelSelectButton.rect.width,button.getVisableId());
			}
		}
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		if(!Assets.schemingWeasel.isPlaying()){
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		LevelSelector.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		LevelSelector.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

}
