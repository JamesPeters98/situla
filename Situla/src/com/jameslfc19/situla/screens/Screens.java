package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class Screens {
	
	public static EnviromentSelect enviromentSelect;
	public static LevelSelector levelSelector;
	public static StartMenu startMenu;
	
	public static void create(Game game){
		Gdx.app.log("SITULA", "Created screens.");
		startMenu = new StartMenu(game);
		enviromentSelect = new EnviromentSelect(game);
		
		levelSelector = new LevelSelector(game);
		
	}

}
