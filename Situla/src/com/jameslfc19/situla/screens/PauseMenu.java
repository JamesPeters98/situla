package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.Input;

public class PauseMenu implements Screen{
	
	Image buttonResume, buttonQuit, buttonOptions;
	Sprite background, background_left, background_right;
	Button resume, quit, options;
	SpriteBatch batch;
	Game game;
	GlobalScores scores;
	OrthographicCamera camera;
	GameScreen prevScreen;
	boolean isPressed;
	int score;
	int highscore;
	int prevGameState;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;

	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	public PauseMenu(Game game, int score, int highscore, GameScreen prevScreen, int prevGameState){
		this.game = game;
		this.score = score;
		this.highscore = highscore;
		this.prevScreen = prevScreen;
		this.prevGameState = prevGameState;
		
		resume = new Button();
		options = new Button();
		quit = new Button();
		
		batch = new SpriteBatch();
		
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		initImages();
	}
	
	private void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		buttonResume = new Image(0, 500, 141, 672);
		buttonOptions = new Image(viewportWidth-650, 500, 141, 672);
		buttonQuit = new Image(viewportWidth-650, 320, 141, 672);
	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Assets.pauseAllMusic();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		Assets.font140.draw(batch, "Paused", viewportWidth /2 - Assets.font140.getBounds("Paused").width/2, viewportHeight - 150);
		Assets.font50.draw(batch, "Score: " + score, 20, 1060);
		Assets.font50.draw(batch, "HighScore: " + highscore, 20, 980);
		if(resume.createTextButton(buttonResume, camera, batch, game, Assets.font60, "Resume", false)){
			prevScreen.resumeGame();
			prevScreen.setGameState(prevGameState);
			game.setScreen((Screen) prevScreen);
		}
		options.createSetScreenButton(buttonOptions, camera, batch, game, Assets.font60, "Options", new Options(game, this), true);
		if(quit.createTextButton(buttonQuit, camera, batch, game, Assets.font60, "Quit", true)){
			Assets.stopMusic(Assets.schemingWeasel);
			game.setScreen(Screens.startMenu);
		}
		batch.end();
	}
	
	public boolean isPressed(){
		return isPressed;
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		PauseMenu.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		PauseMenu.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

}