package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.ImageCentered;
import com.jameslfc19.situla.utils.Input;

public class GameOver implements Screen {
	
	Game game;
	OrthographicCamera camera;
	SpriteBatch batch;
	Image button, mainmenubutton, highscoresbutton, achievemenbutton, sharebutton;
	Sprite background, background_left, background_right; 
	GameScreen screen;
	Button restart, mainmenu, highscores, achievement, share;
	Screen menu;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	int score;
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	public GameOver(Game game, GameScreen screen, int score){
		this.game = game;
		this.screen = screen;
		this.score = score;
		
		restart = new Button();
		mainmenu = new Button();
		highscores = new Button();
		achievement = new Button();
		share = new Button();
				
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		
		initImages();
		
		menu = Screens.startMenu;
	}
	
	private void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		button = new Image(0, 500, 141, 650);
		achievemenbutton = new Image(0, 320, 141, 650);
		sharebutton = new Image(0, 140, 141, 650);
		mainmenubutton = new Image(viewportWidth-672, 500, 141, 650);
		highscoresbutton = new Image(viewportWidth-672, 320, 141, 650);
	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.7f, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		Assets.font140.draw(batch, "Game Over!", screen.getViewportWidth()/2 - Assets.font140.getBounds("Game Over!").width/2, screen.getViewportHeight() - 100);
		Assets.font60.setColor(Color.WHITE);
		Assets.font60.draw(batch, "You scored " + score + " points.", screen.getViewportWidth()/2 - Assets.font60.getBounds("You scored " + score + " points.").width/2, screen.getViewportHeight() - 300);
		if(restart.createSetScreenButton(button, camera, batch, game, Assets.font60, "Restart!", screen.reset(), false)){
			Assets.stopMusic(Assets.schemingWeasel);
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
		if(mainmenu.createSetScreenButton(mainmenubutton, camera, batch, game, Assets.font60, "Main Menu", menu, true)){
			Assets.stopMusic(Assets.schemingWeasel);
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
		if(highscores.createTextButton(highscoresbutton, camera, batch, game, Assets.font60, "Highscores", true)) Situla.GooglePlay.showEndlessLeaderboard();
		if(achievement.createTextButton(achievemenbutton, camera, batch, game, Assets.font60, "Achievements", false)) Situla.GooglePlay.showAchievements();
		if(share.createTextButton(sharebutton, camera, batch, game, Assets.font60, "Share", false)) Situla.share.share(score);
		batch.end();
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		Assets.pauseAllMusic();
		Assets.playMusic("SneakySnitch", Assets.sneakySnitch);
	}

	@Override
	public void hide() {
		Assets.stopMusic(Assets.sneakySnitch);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		GameOver.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		GameOver.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

}
