package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.DropsRegistry;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.drops.Drop;
import com.jameslfc19.situla.drops.Normal;
import com.jameslfc19.situla.games.CampaignGame;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.games.WindGame;
import com.jameslfc19.situla.interfaces.TutorialItem;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.ImageCentered;
import com.jameslfc19.situla.utils.Input;
import com.jameslfc19.situla.utils.Strings;

public class EnviromentSelect implements Screen {
	
	Game game;
	Screen level1;
	OrthographicCamera camera;
	SpriteBatch batch;
	Image room1Image, room2Image, logo, quitbutton;
	Sprite background, background_left, background_right;
	Button back, room1Button, room2Button;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	
	public EnviromentSelect(Game game){
		this.game = game;
		
		room1Button = new Button();
		room2Button = new Button();
		back = new Button();
				
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		
		initImages();
		
		Drop[] level1drops = {new Normal()};
		TutorialItem[] level1tutorialitems = {new TutorialItem(DropsRegistry.Normal), new TutorialItem(Assets.getSprite(Assets.BUCKET_ICON), new Image(0,0,90,72), Strings.BUCKET_DESCRIPTION)};
		level1 = new WindGame(game, 20, 3f, 0.1f, 1.5f, level1drops, level1tutorialitems,1,this);;
	}
	
	private void initImages(){
		background = Assets.getSprite(Assets.BG_BLUE, -(getWidth() - getViewportWidth()) / 2, 0);
		background_left = Assets.getSprite(Assets.BG_BLUE, background.getX() - background.getWidth(), 0);
		background_right = Assets.getSprite(Assets.BG_BLUE, background.getX() + background.getWidth(), 0);
		room1Image = new Image(viewportWidth/2 - 549, 400, 284, 453);
		room2Image = new Image(viewportWidth/2 + 96, 400, 284, 453);
		quitbutton = new Image(viewportWidth-672, 100, 141, 650);
		logo = new ImageCentered().Y(getViewportHeight() - 400, 250, 1200, viewportWidth);
	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.7f, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		background.draw(batch);
		background_left.draw(batch);
		background_right.draw(batch);
		batch.draw(Assets.getFilteredTexture("Logo"), logo.rect.x, logo.rect.y);
		back.createSetScreenButton(quitbutton, camera, batch, game, Assets.font60, "Back", Screens.startMenu, true);
		if(room1Button.createIconButton(room1Image, Assets.getFilteredTexture("Room1Thumbnail"), Assets.getFilteredTexture("Room1ThumbnailPressed"), camera, batch, game)) game.setScreen(Screens.levelSelector);
		if(room2Button.createIconButton(room2Image, Assets.getFilteredTexture("WindLevelThumbnail"), Assets.getFilteredTexture("WindLevelThumbnailPressed"), camera, batch, game)) game.setScreen(level1);
		batch.end();
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		if(!Assets.schemingWeasel.isPlaying()){
			Assets.playMusic("SchemingWeasel", Assets.schemingWeasel);
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		EnviromentSelect.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		EnviromentSelect.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

}
