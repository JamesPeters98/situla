package com.jameslfc19.situla.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.GlobalScores;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.games.GameScreen;
import com.jameslfc19.situla.utils.Button;
import com.jameslfc19.situla.utils.Image;
import com.jameslfc19.situla.utils.Input;

public class LoadingScreen implements Screen {
	
	Game game;
	OrthographicCamera camera;
	SpriteBatch batch;
	Image background;
	
	int physicalWidth = Gdx.graphics.getWidth();
	int physicalHeight = Gdx.graphics.getHeight();
	float aspect;
	static float viewportHeight;
	static float viewportWidth;
	final static int WIDTH = 1920;
	final static int HEIGHT = 1080;
	boolean AssetsLoaded = false;
	
	public LoadingScreen(Game game){
		this.game = game;				
		calculateAspectRatio();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
		
		batch = new SpriteBatch();
		initImages();
	}
	

	private void initImages(){
		background = new Image(-(getWidth() - getViewportWidth()) / 2, 0, getHeight(), getWidth());

	}

	@Override
	public void render(float delta) {
		Input.checkResToggle(game);
		Gdx.gl.glClearColor(0, 0.671875f, 0.921875f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(Assets.Splash, background.rect.x, background.rect.y);
		batch.end();
		
		if(Assets.doneLoading() && !AssetsLoaded){
			Screens.create(game);
			game.setScreen(Screens.startMenu);
			AssetsLoaded = true;
		} 
	}
	
	private void calculateAspectRatio(){
		aspect = WIDTH / HEIGHT;

		if (physicalWidth / physicalHeight >= aspect) {
			viewportHeight = HEIGHT;
			viewportWidth = viewportHeight * physicalWidth / physicalHeight;
		} else {
			viewportWidth = WIDTH;
			viewportHeight = viewportWidth * physicalHeight / physicalWidth;
		}
	}

	@Override
	public void resize(int width, int height) {
		physicalWidth = Gdx.graphics.getWidth();
		physicalHeight = Gdx.graphics.getHeight();
		calculateAspectRatio();
		initImages();
		camera.setToOrtho(false, getViewportWidth(), getViewportHeight());
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
/* Getters & Setters */
	
	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		LoadingScreen.viewportHeight = viewportHeight;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		LoadingScreen.viewportWidth = viewportWidth;
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

}
