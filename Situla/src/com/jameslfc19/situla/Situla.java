package com.jameslfc19.situla;

import java.io.IOException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jameslfc19.situla.assets.Assets;
import com.jameslfc19.situla.assets.Files;
import com.jameslfc19.situla.interfaces.GooglePlayInterface;
import com.jameslfc19.situla.interfaces.Share;
import com.jameslfc19.situla.screens.LoadingScreen;
import com.jameslfc19.situla.screens.Screens;
import com.jameslfc19.situla.screens.StartMenu;

public class Situla extends Game implements Screen {
	
	public final static String key = "fTYYw8pcfO8TbSNt";
	public final static String IV = "6f08132400e9df3f";
	
	FPSLogger fps;
	boolean AssetsLoaded = false; 
	SpriteBatch batch;
	Texture Splash;
	LevelData leveldata;
	boolean doneLoading = false;
	
	public static GooglePlayInterface GooglePlay;
	public static Share share;
	
	public Situla(GooglePlayInterface googlePlay, Share share){
		GooglePlay = googlePlay;
		this.share = share;
		
	}

	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		Assets.load();
		DropsRegistry.register();
		if (!Files.doesFileExist("score.dat")) {
			Files.writeToFile("score.dat", "0", true);
		}
		if (!Files.doesFileExist("LevelData.dat")) {
			try {
				Files.writeLevelDataJson();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			Files.readLevelDataJson();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (!Files.doesFileExist("Preferences.json")) {
			try {
				Files.generatePreferenceJson();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			Files.readPreferenceJson();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setScreen(new LoadingScreen(this));
		
		fps = new FPSLogger();
		batch = new SpriteBatch();
		
	}
	
	@Override
	public void render(){
		super.render();
		//fps.log();
		if(Assets.doneLoading() && doneLoading == false){
			//Screens.create(this);
			doneLoading = true;
		}
	}

	@Override
	public void dispose () {
		super.dispose();

		getScreen().dispose();
	}

	@Override
	public void render(float delta) {
		
	}

	@Override
	public void show() {
		Splash = new Texture(Gdx.files.internal("data/res/splashScreen.png"));
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

}
