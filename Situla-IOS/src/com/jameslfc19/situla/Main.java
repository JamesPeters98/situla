package com.jameslfc19.situla;

import org.robovm.cocoatouch.foundation.NSAutoreleasePool;
import org.robovm.cocoatouch.uikit.UIApplication;
import org.robovm.cocoatouch.uikit.UIInterfaceOrientation;
import org.robovm.cocoatouch.uikit.UIStatusBarAnimation;
import org.robovm.cocoatouch.uikit.UIStatusBarStyle;
import org.robovm.cocoatouch.uikit.UIViewController;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class Main extends IOSApplication.Delegate {
		
	@Override
	protected IOSApplication createApplication () {
		UIApplication.getSharedApplication().setStatusBarHidden(true, UIStatusBarAnimation.None);
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		IOSApplication app;
		config.orientationLandscape = true;
		config.orientationPortrait = false;
		config.displayScaleSmallScreenIfRetina = 0.5f;
		config.useAccelerometer = true;
		config.preventScreenDimming = true;
		config.accelerometerUpdate = 0.05f;
		app = new IOSApplication(new Situla(new GooglePlayIOS(), new ShareIOS()), config);
		return app;
	}

	public static void main (String[] argv) {
		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, Main.class);
		pool.drain();
	}
}
