package com.jameslfc19.situla;

import org.robovm.cocoatouch.foundation.NSArray;
import org.robovm.cocoatouch.foundation.NSString;
import org.robovm.cocoatouch.uikit.UIActivityViewController;
import org.robovm.cocoatouch.uikit.UIApplication;
import org.robovm.cocoatouch.uikit.UIViewController;
import org.robovm.cocoatouch.uikit.UIWindow;
import org.robovm.objc.block.VoidBlock;

import com.jameslfc19.situla.interfaces.Share;

public class ShareIOS implements Share{
	
	VoidBlock voidBlock;
	
	public ShareIOS(){
		voidBlock = new VoidBlock(){

			@Override
			public void invoke() {
				
			}
			
		};
	}

	@Override
	public void share(int score) {
		NSString shareText = new NSString("Some Text");
		NSArray<NSString> dataToShare = new NSArray<NSString>(shareText);
		UIActivityViewController viewController = new UIActivityViewController(dataToShare, null);
		UIApplication.getSharedApplication().getKeyWindow().getRootViewController().presentViewController(viewController, true, voidBlock);
	}

}
