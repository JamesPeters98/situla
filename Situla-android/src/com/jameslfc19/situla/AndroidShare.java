package com.jameslfc19.situla;

import android.app.Activity;
import android.content.Intent;

import com.jameslfc19.situla.interfaces.Share;

public class AndroidShare implements Share {
	
	private Activity activity;
	
	public AndroidShare(Activity activity){
		this.activity = activity;
	}
	

	@Override
	public void share(int score) {
		String message = "I just played Situla and scored a score of "+ score + ". Play it now at %situlalinkgoeshere%";
		
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, "I just played Situla!");
		
		shareIntent.putExtra(Intent.EXTRA_TEXT, message);
		
		activity.startActivity(Intent.createChooser(shareIntent, "Share Situla!"));
	}

}
