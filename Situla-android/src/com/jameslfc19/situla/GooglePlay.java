package com.jameslfc19.situla;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.OnScoreSubmittedListener;
import com.google.android.gms.games.leaderboard.SubmitScoreResult;
import com.jameslfc19.situla.googleplayservices.GameHelper;
import com.jameslfc19.situla.googleplayservices.GooglePlayAndroid;
import com.jameslfc19.situla.interfaces.GooglePlayInterface;

public class GooglePlay extends GooglePlayAndroid implements GooglePlayInterface, OnScoreSubmittedListener{

	private GameHelper aHelper;
	private Activity activity;

	public GooglePlay(Activity activity, GameHelper gameHelper){
		aHelper =  gameHelper;
		this.activity = activity;
	}
	
	@Override
	protected void onCreate(Bundle b) {
		// TODO Auto-generated method stub
		super.onCreate(b);
		aHelper.setup(this);
	}
	
	@Override
	public void userSignIn() {
		if(!aHelper.isSignedIn()){
			try {
				runOnUiThread(new Runnable(){
	
				//@Override
				public void run(){
					aHelper.beginUserInitiatedSignIn();
				}
				});
				}catch (final Exception ex){
	
				}
		} else {
			System.out.println("Already signed in!");
		}
	}
	

	@Override
	public void userSignOut() {
		aHelper.signOut();
	}

	@Override
	public void onSignInFailed() {
		System.out.print("Sign in failed!");
	}

	@Override
	public void onSignInSucceeded() {
		System.out.print("Sign in succeeded!");
	}

	@Override
	public void submitEndlessHighscore(long score) {
		if(isLoggedIn()) aHelper.getGamesClient().submitScoreImmediate(this, EndlessLeaderboard, score);
	}

	@Override
	public void showEndlessLeaderboard() {
		if(isLoggedIn()) activity.startActivityForResult(aHelper.getGamesClient().getLeaderboardIntent(EndlessLeaderboard), 1);
		else {
			userSignIn();
		}
	}

	@Override
	public void onScoreSubmitted(int statusCode, SubmitScoreResult result) {
		System.out.println(result.getScoreResult(2).formattedScore);
	}

	@Override
	public boolean isLoggedIn() {
		return aHelper.isSignedIn();
	}

	@Override
	public void unlockAchievement(String id) {
		if(isSignedIn()) aHelper.getGamesClient().unlockAchievement(id);
	}

	@Override
	public void showAchievements() {
		if(isLoggedIn()) activity.startActivityForResult(aHelper.getGamesClient().getAchievementsIntent(), 1);
		else {
			userSignIn();
		}
	}
	
}
