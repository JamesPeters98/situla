package com.jameslfc19.situla;

import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.crashlytics.android.Crashlytics;
import com.jameslfc19.situla.Situla;
import com.jameslfc19.situla.googleplayservices.GameHelper;
import com.jameslfc19.situla.googleplayservices.GameHelper.GameHelperListener;

public class MainActivity extends AndroidApplication implements GameHelperListener {
	
	private GameHelper gamehelper;
	
	public MainActivity(){
		gamehelper = new GameHelper(this);
		gamehelper.enableDebugLog(true, "SITULA");
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        
        Crashlytics.setUserIdentifier(deviceId);
        
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        cfg.useAccelerometer = true;
        cfg.useCompass = false;
        cfg.useWakelock = true;
        
        gamehelper.setup(this);
        
        initialize(new Situla(new GooglePlay(this, gamehelper), new AndroidShare(this)), cfg);
        
    }

	@Override
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		gamehelper.onActivityResult(request, response, data);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		gamehelper.onStart(this);
	}

	@Override
	public void onStop(){
		super.onStop();
		gamehelper.onStop();
	}
}